package dk.digst.digital.post.memolib.builder;

import dk.digst.digital.post.memolib.model.Address;
import dk.digst.digital.post.memolib.model.AddressPoint;
import dk.digst.digital.post.memolib.model.AttentionPerson;
import dk.digst.digital.post.memolib.model.ContentResponsible;
import dk.digst.digital.post.memolib.model.EidData;
import dk.digst.digital.post.memolib.model.Email;
import dk.digst.digital.post.memolib.model.GeneratingSystem;
import dk.digst.digital.post.memolib.model.GlobalLocationNumber;
import dk.digst.digital.post.memolib.model.MessageType;
import dk.digst.digital.post.memolib.model.ProductionUnit;
import dk.digst.digital.post.memolib.model.SeNumber;
import dk.digst.digital.post.memolib.model.SorData;
import dk.digst.digital.post.memolib.model.Telephone;
import dk.digst.digital.post.memolib.model.UnstructuredAddress;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.UUID;

/**
 * This class provides builders for the Core model classes and should only be used by automated
 * tests. The builders has been pre-filled with test data, but can be customized afterwards.
 */
public class PrefilledMessageBuilderFactory {
  private PrefilledMessageBuilderFactory() {}

  public static MessageBuilder message() {
    return MessageBuilder.newBuilder()
        .messageHeader(messageHeader().build())
        .messageBody(messageBody().build());
  }

  public static MessageHeaderBuilder messageHeader() {
    return MessageHeaderBuilder.newBuilder()
        .messageType(MessageType.DIGITALPOST)
        .messageUUID(UUID.randomUUID())
        .messageId("someMessageID")
        .messageCode("someMessageCode")
        .label("someLabel & some other")
        .notification("someNotification")
        .reply(false)
        .replyByDateTime(LocalDateTime.parse("2100-02-03T12:00:00"))
        .doNotDeliverUntilDate(LocalDate.parse("2100-10-10"))
        .mandatory(false)
        .legalNotification(false)
        .postType("somePostType")
        .sender(sender().build())
        .recipient(recipient().build())
        .replyData(Collections.singletonList(replyData().build()))
        .replyData(Collections.singletonList(replyData().build()))
        .forwardData(forwardData().build())
        .contentData(contentData().build());
  }

  public static ForwardDataBuilder forwardData() {
    return ForwardDataBuilder.newBuilder()
        .messageUUID(UUID.randomUUID())
        .originalMessageDateTime(LocalDateTime.parse("2100-02-03T12:00:00"))
        .originalSender("someOriginalSender")
        .originalContentResponsible("someOriginalContentResponsible")
        .contactPointId("someContactPointId")
        .comment("someComment");
  }

  public static SenderBuilder sender() {
    return SenderBuilder.newBuilder()
        .senderId("someSenderID")
        .idType("someSenderIDtype")
        .idTypeLabel("someSenderIdTypeLabel")
        .label("someSenderLabel")
        .attentionData(attentionData(true).build())
        .contactPoint(contactPoint().build());
  }

  public static RecipientBuilder recipient() {
    return RecipientBuilder.newBuilder()
        .recipientId("someRecipientID")
        .idType("someRecipientIdType")
        .idTypeLabel("someRecipientIdTypeLabel")
        .label("someRecipientLabel")
        .attentionData(attentionData(false).build())
        .contactPoint(contactPoint().build());
  }

  public static AttentionDataBuilder attentionData(boolean usingAddress) {
    AttentionPerson testAttentionPerson =
        AttentionPersonBuilder.newBuilder()
            .personId("somePersonId")
            .label("someAttentionPersonLabel")
            .build();

    ProductionUnit testProductionUnit =
        ProductionUnitBuilder.newBuilder()
            .productionUnitNumber(666)
            .productionUnitName("someProductionUnitName")
            .build();

    GlobalLocationNumber testGlobalLocationNumber =
        GlobalLocationNumberBuilder.newBuilder()
            .globalLocationNumber(9999999999999L)
            .location("someLocation")
            .build();

    Email testEmail =
        EmailBuilder.newBuilder()
            .emailAddress("someEmailAddress")
            .relatedAgent("someRelatedAgent")
            .build();

    SeNumber testSEnumber =
        SeNumberBuilder.newBuilder().seNumber("911").companyName("someCompanyName").build();

    Telephone testTelephone =
        TelephoneBuilder.newBuilder()
            .telephoneNumber("88776655")
            .relatedAgent("someRelatedAgent")
            .build();

    EidData testRIDdate =
        EidDataBuilder.newBuilder().eid("someRIDnumber").label("someCompanyName").build();

    ContentResponsible testContentResponsible =
        ContentResponsibleBuilder.newBuilder()
            .contentResponsibleId("someContentResponsibleID")
            .label("someContentResponsibleLabel")
            .build();

    GeneratingSystem testGeneratingSystem =
        GeneratingSystemBuilder.newBuilder()
            .generatingSystemId("someGeneratingSystemID")
            .label("someGeneratingSystemLabel")
            .build();

    SorData sorData =
        SorDataBuilder.newBuilder().sorIdentifier("somecode").entryName("someName").build();

    Address testAddress = address().build();

    AttentionDataBuilder attentionDataBuilder =
        AttentionDataBuilder.newBuilder()
            .attentionPerson(testAttentionPerson)
            .productionUnit(testProductionUnit)
            .globalLocationNumber(testGlobalLocationNumber)
            .email(testEmail)
            .senumber(testSEnumber)
            .telephone(testTelephone)
            .ridData(testRIDdate)
            .sorData(sorData)
            .contentResponsible(testContentResponsible)
            .generatingSystem(testGeneratingSystem);
    if (usingAddress) {
      return attentionDataBuilder.address(testAddress);
    } else {
      UnstructuredAddress testUnstructuredAddress =
          UnstructuredAddressBuilder.newBuilder().unstructured("someUnstructuredAddress").build();
      return attentionDataBuilder.unstructuredAddress(testUnstructuredAddress);
    }
  }

  private static AddressPoint addressPoint() {
    return AddressPointBuilder.newBuilder()
        .geographicEastingMeasure("someGeographicEastMeasure")
        .geographicNorthingMeasure("someGeographicNorthingMeasure")
        .geographicHeightMeasure("someGeographicHeightMeasure")
        .build();
  }

  private static AddressBuilder address() {
    return AddressBuilder.newBuilder()
        .id(UUID.randomUUID().toString())
        .addressLabel("someAddressLabel")
        .houseNumber("somehouseNumber")
        .door("someDoor")
        .floor("someFloor")
        .co("someCO")
        .zipCode("9999")
        .city("someCity")
        .country("someCountry")
        .addressPoint(addressPoint());
  }

  public static ReservationBuilder reservation() {
    return ReservationBuilder.newBuilder()
        .description("someReservationDescription")
        .reservationUUID(UUID.randomUUID())
        .reservationAbstract("someReservationAbstract")
        .location("someLocation")
        .startDateTime(LocalDateTime.parse("2100-06-03T12:00:00"))
        .endDateTime(LocalDateTime.parse("2100-04-03T12:00:00"))
        .organizerMail("someOrganizerMail")
        .organizerName("someOrganizerName");
  }

  public static EntryPointBuilder entryPoint() {
    return EntryPointBuilder.newBuilder().url("someURL");
  }

  public static ContactInfoBuilder contactInfo() {
    return ContactInfoBuilder.newBuilder()
        .label("someContactInfoLabel")
        .value("someContactInfoValue");
  }

  public static ContactPointBuilder contactPoint() {
    return ContactPointBuilder.newBuilder()
        .contactGroup("someContactGroup")
        .contactPointId("someContactPointID")
        .label("someContactPointLabel")
        .contactInfo(Collections.singletonList(contactInfo().build()));
  }

  public static ReplyDataBuilder replyData() {
    return ReplyDataBuilder.newBuilder()
        .messageId("someReplyDataMessageId")
        .messageUUID(UUID.randomUUID())
        .replyUUID(UUID.randomUUID())
        .senderId("someReplyDataSenderID")
        .recipientId("someReplyDataRecipientID")
        .caseId("someReplyDataCaseID")
        .contactPointId("someReplyDataContactPoint")
        .generatingSystemId("someReplyDataGeneratingSystemId")
        .comment("someReplyDataComment")
        .addAdditionalReplyData(
            AdditionalReplyDataBuilder.newBuilder().label("key1").value("value1").build())
        .addAdditionalReplyData(
            AdditionalReplyDataBuilder.newBuilder().label("key").value("value").build());
  }

  public static MessageBodyBuilder messageBody() {
    return MessageBodyBuilder.newBuilder()
        .createdDateTime(LocalDateTime.parse("2100-05-03T12:00:00"))
        .mainDocument(mainDocument().build())
        .addAdditionalDocument(additionalDocument().build())
        .addTechnicalDocument(technicalDocument().build());
  }

  public static MainDocumentBuilder mainDocument() {
    return MainDocumentBuilder.newBuilder()
        .mainDocumentId("someMainDocumentID")
        .label("someDigitalDocumentLabel & another")
        .addFile(file().build())
        .addFile(file().build())
        .addAction(action(true).build());
  }

  public static AdditionalDocumentBuilder additionalDocument() {
    return AdditionalDocumentBuilder.newBuilder()
        .additionalDocumentId("someAdditionalDocumentID")
        .label("someDigitalDocumentLabel & another")
        .addFile(file().build())
        .addFile(file().build())
        .addAction(action(true).build());
  }

  public static TechnicalDocumentBuilder technicalDocument() {
    return TechnicalDocumentBuilder.newBuilder()
        .technicalDocumentId("someTechnicalDocumentID")
        .label("someDigitalDocumentLabel & another")
        .addFile(file().build())
        .addFile(file().build());
  }

  public static FileBuilder file() {
    return FileBuilder.newBuilder()
        .encodingFormat("application/pdf")
        .filename("model.pdf")
        .inLanguage("da")
        .content(fileContent().build());
  }

  public static FileContentBuilder fileContent() {
    return FileContentBuilder.newBuilder().base64Content("JVBE");
  }

  public static ActionBuilder action(boolean usingReservation) {
    ActionBuilder actionBuilder =
        ActionBuilder.newBuilder()
            .label("someActionLabel & another")
            .actionCode("someActionCode & another")
            .startDateTime(LocalDateTime.parse("2110-05-03T12:00:00"))
            .endDateTime(LocalDateTime.parse("2120-05-03T12:00:00"));
    if (usingReservation) {
      return actionBuilder.reservation(reservation().build());
    } else {
      return actionBuilder.entryPoint(entryPoint().build());
    }
  }

  public static ContentDataBuilder contentData() {
    return ContentDataBuilder.newBuilder()
        .cprData("12424578", "Hans Hansen")
        .cvrData("9857575", "Sjov & løjer ApS")
        .motorVehicle("YT58472", "a92734lad8222AAA")
        .propertyNumber("AB75")
        .caseId("75633838373", "ESDH")
        .kleData("Some subject & another", "2", "facet1 & facet2", "label1 & label2")
        .formData("some task", "7", "facet3 & facet4", "label3 & label4")
        .productionUnit(272644, "some unit name")
        .education("635EAS", "some education & another")
        .address(address().build())
        .addAdditionalContentData(additionalContentData().build())
        .addAdditionalContentData(additionalContentData().build())
        .addAdditionalContentData(additionalContentData().build());
  }

  public static AdditionalContentDataBuilder additionalContentData() {
    return new AdditionalContentDataBuilder()
        .contentDataType("content data type1 & type2")
        .contentDataName("content data name1 & name2")
        .contentDataValue("content data value1 & value2");
  }
}
