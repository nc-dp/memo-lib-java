package dk.digst.digital.post.memolib.container


import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.util.MeMoTestDataProvider
import spock.lang.Shared
import spock.lang.Specification

class MeMoContainerReaderTarLzmaSpec extends Specification {

  @Shared private MeMoContainerReader meMoContainerReader

  @Shared private FileInputStream fileInputStream

  @Shared private IterableContainer container

  void setup() {
    fileInputStream = MeMoTestDataProvider.streamMeMoContainerTarLzmaWithTwoMessages()
    meMoContainerReader =  new MeMoContainerReaderFactory().newContainerReader(fileInputStream)
  }

  void 'memo archive should have next entry initially'() {
    when:
    boolean hasEntry = meMoContainerReader.hasEntry()
    then:
    hasEntry
    cleanup:
    meMoContainerReader.close()
  }

  void 'parsing two messages from the memo archive'() {
    when:
    Optional<Message> firstMessage = meMoContainerReader.readEntry()
    Optional<Message> secondMessage = meMoContainerReader.readEntry()
    Optional<Message> thirdMessage = meMoContainerReader.readEntry()
    then:
    firstMessage.isPresent()
    firstMessage.get().messageHeader.messageId == 'MSG-12345'
    secondMessage.isPresent()
    secondMessage.get().messageHeader.messageId == 'MSG-22222'
    !thirdMessage.isPresent()
    cleanup:
    meMoContainerReader.close()
  }

  void 'the reader closes the file input stream'() {
    when:
    meMoContainerReader.close()
    fileInputStream.available()
    then:
    IOException expectedException = thrown()
    expectedException.message == 'Stream Closed'
  }

  void 'the constructor throws an exception if the MeMoContainer is null'() {
    when:
    new MeMoContainerReader(null)
    then:
    thrown NullPointerException
  }

  void 'fail if dictionary size is too large'() {
    when:
    fileInputStream = MeMoTestDataProvider.streamMeMoContainerTarLzmaWithTwoMessagesTooLargeDict()
    meMoContainerReader = new MeMoContainerReaderFactory().newContainerReader(fileInputStream)

    then:
    IllegalStateException expectedException = thrown()
    expectedException.message == 'Dictionary size is too large - unsupported! (dictionary size: 262144 kB)'
  }

  void 'read memo created on command line'() {
    when:
    fileInputStream = MeMoTestDataProvider.streamMeMoContainerTarLzmaFromCommandLine()
    meMoContainerReader = new MeMoContainerReaderFactory().newContainerReader(fileInputStream)

    int i = 0
    while (meMoContainerReader.hasEntry()) {
      meMoContainerReader.readEntry()
      i++
    }

    then:
    i == 2
  }
  
  void 'read memo created on command line with 7zip on windows'() {
    when:
    fileInputStream = MeMoTestDataProvider.streamMeMoContainerTarLzma7zWindowsFromCommandLine()
    meMoContainerReader = new MeMoContainerReaderFactory().newContainerReader(fileInputStream)

    int i = 0
    while (meMoContainerReader.hasEntry()) {
      meMoContainerReader.readEntry()
      i++
    }

    then:
    i == 2
  }
}
