package dk.digst.digital.post.memolib.util


import spock.lang.Shared
import spock.lang.Specification

class Base64StreamEncoderSpec extends Specification {

  @Shared private Base64.Decoder decoder

  void setup() {
    decoder = Base64.getDecoder()
  }

  void "A byte array representing a sequence of ascii characters should be able to be encoded as Base64"(String input) {
    given: "a byte array of ascii characters"
    byte[] inputBytes = input.bytes

    InputStream inputStream = new ByteArrayInputStream(inputBytes)
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream()

    when: "the bytes are encoded as Base64"
    Base64StreamEncoder.encodeAsBase64(inputStream, outputStream)

    then: "the output can be decoded to the original byte array of ascii characters"
    byte[] decodedBytes = decoder.decode(outputStream.toByteArray())
    Arrays.equals(inputBytes, decodedBytes)

    where:
    input                         | _
    "JVBE"                        | _
    "JVBE1"                       | _
    "JVæ7Å8"                      | _
    "JVæ7Å8!"                     | _
    "-Væ7Å8!0"                    | _
    "-Væ7Å8!0q/"                  | _
    "-Væ7Å8!0q/["                 | _
    "-Væ7Å8!0q/==["               | _
    "-Væ7Å8!£%ajsdhu="            | _
    "-Væ3478936&&allæfd788__."    | _
    "ABDO27D¤#S0E9(/&d8fjadf6287" | _
  }

  void "A binary file should be able to be encoded as Base64"() {
    given: "a pdf file"
    InputStream inputStream = new FileInputStream(MeMoTestDataProvider.modelPdf())
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream()

    when: "the bytes are encoded as Base64"
    Base64StreamEncoder.encodeAsBase64(inputStream, outputStream)

    then: "the output can be decoded to the original file content"
    InputStream stream = new FileInputStream(MeMoTestDataProvider.modelPdf())
    byte[] inputBytes =  new byte[stream.available()]
    stream.read(inputBytes)
    byte[] decodedBytes = decoder.decode(outputStream.toByteArray())
    Arrays.equals(inputBytes, decodedBytes)

    cleanup:
    inputStream.close()
  }
}
