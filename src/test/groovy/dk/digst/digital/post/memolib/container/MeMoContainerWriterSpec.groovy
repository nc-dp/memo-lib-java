package dk.digst.digital.post.memolib.container

import dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import spock.lang.Shared
import spock.lang.Specification

class MeMoContainerWriterSpec extends Specification {

  public static final String MEMO = 'memo.xml'

  @Shared private MeMoContainerWriter containerWriter

  @Shared private ContainerOutputStream meMoContainer = Mock(ContainerOutputStream)

  @Shared private MeMoNewContainerWriter newContainerWriter

  void setup() {
    meMoContainer = Mock(ContainerOutputStream)
    containerWriter = new MeMoContainerWriter(meMoContainer)
    newContainerWriter = new MeMoNewContainerWriter(meMoContainer)
  }

  void cleanup() {
    containerWriter.close()
  }

  void 'throws IOException if the writer is closed before any XML elements has been written'() {
    given:
    String fileName = MEMO
    ByteArrayOutputStream output = new ByteArrayOutputStream()
    when:
    MeMoStreamWriter memoWriter = containerWriter.getWriterForNextEntry(fileName) as MeMoStreamWriter
    memoWriter.close()
    then:
    1 * meMoContainer.writeNextEntry(fileName) >> output
    memoWriter.isClosed()
    thrown IOException
  }

  void 'closes the previous entry, correctly before writing the next one'() {
    given:
    Message message = PrefilledMessageBuilderFactory.message().build()
    String fileName1 = 'memo1.xml'
    String fileName2 = 'memo2.xml'
    ByteArrayOutputStream firstOutput = new ByteArrayOutputStream()
    ByteArrayOutputStream secondOutput = new ByteArrayOutputStream()

    when: 'writing first MeMo message'
    MeMoStreamWriter firstWriter = containerWriter.getWriterForNextEntry(fileName1)
    firstWriter.write(message)
    and: 'writing second MeMo message'
    MeMoStreamWriter secondWriter = containerWriter.getWriterForNextEntry(fileName2)
    secondWriter.write(message)
    and: 'close the second writer to flush the written content'
    secondWriter.close()
    then:
    1 * meMoContainer.writeNextEntry(fileName1) >> firstOutput
    1 * meMoContainer.writeNextEntry(fileName2) >> secondOutput
    and: 'both messages ends with the expected closing tag'
    firstOutput.toString().endsWith('</memo:Message>')
    secondOutput.toString().endsWith('</memo:Message>')
  }

  void 'write a complete entry given a full message object as parameter'() {
    given:
    String fileName = MEMO
    ByteArrayOutputStream output = new ByteArrayOutputStream()
    Message message = PrefilledMessageBuilderFactory.message().build()
    when:
    containerWriter.writeEntry(fileName, message)
    then:
    1 * meMoContainer.writeNextEntry(fileName) >> output
    1 * meMoContainer.closeEntry()
  }

  void 'write a complete entry given a full message object as parameter with new MeMo '() {
    given:
    String fileName = MEMO
    ByteArrayOutputStream output = new ByteArrayOutputStream()
    Message message = PrefilledMessageBuilderFactory.message().build()
    when:
    newContainerWriter.writeEntry(fileName, message)
    then:
    1 * meMoContainer.writeNextEntry(fileName) >> output
    1 * meMoContainer.closeEntry()
  }

  void 'New contain writer closes the previous entry correctly, before writing the next one'() {
    given:
    Message message = PrefilledMessageBuilderFactory.message().build()
    String fileName1 = 'memo1.xml'
    String fileName2 = 'memo2.xml'
    ByteArrayOutputStream firstOutput = new ByteArrayOutputStream()
    ByteArrayOutputStream secondOutput = new ByteArrayOutputStream()

    when: 'writing first MeMo message'
    MeMoStreamWriter firstWriter = newContainerWriter.getWriterForNextEntry(fileName1)
    firstWriter.write(message)

    and: 'writing second MeMo message'
    MeMoStreamWriter secondWriter = newContainerWriter.getWriterForNextEntry(fileName2)
    secondWriter.write(message)

    and: 'close the second writer to flush the written content'
    secondWriter.close()

    then:
    1 * meMoContainer.writeNextEntry(fileName1) >> firstOutput
    1 * meMoContainer.writeNextEntry(fileName2) >> secondOutput

    and: 'both messages ends with the expected closing tag'
    firstOutput.toString().endsWith('</memo:Message>')
    secondOutput.toString().endsWith('</memo:Message>')

    and: "messages is of new version"
    MeMoVersion.MEMO_VERSION_NEW.test(MeMoParserFactory.createNewParser(new ByteArrayInputStream(firstOutput.toByteArray()), true).parse().memoVersion)
    MeMoVersion.MEMO_VERSION_NEW.test(MeMoParserFactory.createNewParser(new ByteArrayInputStream(secondOutput.toByteArray()), true).parse().memoVersion)
  }
}
