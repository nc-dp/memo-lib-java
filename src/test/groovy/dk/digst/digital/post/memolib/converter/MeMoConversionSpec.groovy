package dk.digst.digital.post.memolib.converter

import java.time.LocalDateTime
import dk.digst.digital.post.memolib.config.GlobalConfig
import dk.digst.digital.post.memolib.converter.rules.DownConvertHeaderRule
import dk.digst.digital.post.memolib.model.ForwardData
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageHeader
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.memolib.util.MeMoSchemaTestProvider
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitorFactory
import spock.lang.Issue
import spock.lang.Specification

class MeMoConversionSpec extends Specification {

  private MeMoConverter upConverter

  private MeMoConverter downConverter

  private final String XML_DIR = "src/test/resources/xml/"

  private MessageHeader header

  void setup() {
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    File memoNew = new File(XML_DIR + "MeMo_Full_Example_V1.2.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)
    FileInputStream fileInputStreamNew = new FileInputStream(memoNew)
    header = getMessageHeader(memo)

    upConverter = MeMoConvertionFactory.createUpConverter(fileInputStream)
    downConverter = MeMoConvertionFactory.createDownConverter(fileInputStreamNew)
  }

  @Issue(['AC-6199', 'AC-6200'])
  void "When converting a 1.1 version MeMo to MeMo version 1.2, then it will successfully be converted with a correct version without throwing exception. And the fields memoSchversion and representative won't exists"() {
    when:
    Message result = upConverter.parseIntoConvertedObject()

    then:
    noExceptionThrown()

    and:
    result
    MeMoVersion.MEMO_VERSION_NEW == MeMoVersion.fromValue(result.memoVersion)
    !result.memoSchVersion
    !result.messageHeader.sender.representative
  }

  @Issue(['AC-6202', 'AC-6201', 'AC-6205'])
  void "When converting a 1.2 version MeMo to MeMo version 1.1, then it will successfully be converted with a correct version and fields, without throwing exception"() {
    when:
    Message result = downConverter.parseIntoConvertedObject()

    then:
    noExceptionThrown()

    and:
    result
    MeMoVersion.fromValue(result.memoVersion) == MeMoVersion.MEMO_VERSION_OLD
    result.memoSchVersion == MeMoVersion.MEMO_SCH_VERSION
    !result.messageHeader.sender.representative
  }

  @Issue(['AC-6203', 'AC-6204'])
  void "When converting a 1.2 version MeMo to MeMo version 1.1, and legalNotification and mandatory do not exist, expect them to be false after conversion"() {
    given:
    MeMoConverter converter = MeMoConvertionFactory.createDownConverter(new FileInputStream(new File(XML_DIR + 'MeMo_Minimum_Example_V1.2.xml')), false)

    expect:
    Message source = MeMoParserFactory.createParser(new FileInputStream(new File(XML_DIR + 'MeMo_Minimum_Example_V1.2.xml'))).parse()
    source.messageHeader.mandatory == null
    source.messageHeader.legalNotification == null

    when:
    Message result = converter.parseIntoConvertedObject()

    then:
    noExceptionThrown()

    and:
    result
    MeMoVersion.fromValue(result.memoVersion) == MeMoVersion.MEMO_VERSION_OLD
    result.memoSchVersion == MeMoVersion.MEMO_SCH_VERSION
    result.messageHeader.mandatory == false
    result.messageHeader.legalNotification == false
  }

  void "When converting a 1.2 version MeMo to MeMo version 1.1, it will create a valid  inputstream"() {
    when:
    InputStream result = downConverter.parseIntoConvertedStream()

    then:
    noExceptionThrown()

    and:
    MeMoSchemaTestProvider.getXmlSchemaValidator().isValid(result)
  }

  void "When converting a 1.1 version MeMo to MeMo version 1.2, it will create a valid inputstream"() {
    when:
    InputStream result = upConverter.parseIntoConvertedStream()

    then:
    noExceptionThrown()

    and:
    MeMoSchemaTestProvider.getNewXmlSchemaValidator().isValid(result)
  }

  @Issue("AC-6292")
  void "When converting a 1.1 version MeMo to MeMo version 1.2, and reply=true and replyData is empty, set a valid ReplyData"() {
    given:
    MeMoConverter converter = MeMoConvertionFactory.createUpConverter(new FileInputStream(new File(XML_DIR + 'MeMo_Invalid_Missing_ReplyData_V1.1.xml')), false)

    expect:
    Message source = MeMoParserFactory.createParser(new FileInputStream(new File(XML_DIR + 'MeMo_Invalid_Missing_ReplyData_V1.1.xml'))).parse()
    source.messageHeader.replyData == null

    when:
    Message result = converter.parseIntoConvertedObject()

    then:
    noExceptionThrown()

    and:
    result
    result.messageHeader.replyData != null
    result.messageHeader.replyData[0].messageUUID == result.messageHeader.messageUUID
  }

  private static MessageHeader getMessageHeader(File memo) {
    FileInputStream fileInputStream = new FileInputStream(memo)
    MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, false)
    def visitor = MeMoStreamVisitorFactory.createProcessor(MessageHeader)
    parser.traverse(visitor)

    return visitor.getSingleResult().orElse(null)
  }

  void "Down conversion: When ForwardData has originalRepresentative, it should be moved to originalContentResponsible and set to null"() {
    given: "A message header with ForwardData containing originalRepresentative"
    MessageHeader header = new MessageHeader()
    ForwardData forwardData = new ForwardData(
            UUID.randomUUID(),
            LocalDateTime.now(),
            "originalSender",
            null,
            "12345",
            "contactPointId",
            "comment"
    )
    header.setForwardData(forwardData)

    and: "A converter rule"
    DownConvertHeaderRule rule = new DownConvertHeaderRule()
    Message message = new Message()
    message.setMessageHeader(header)

    when: "Converting the message"
    rule.convert(message)

    then: "originalRepresentative should be moved to originalContentResponsible and set to null"
    MessageHeader convertedHeader = message.getMessageHeader()
    convertedHeader.getForwardData().getOriginalContentResponsible() == "12345"
    convertedHeader.getForwardData().getOriginalRepresentative() == null
  }

  void "Down conversion: When ForwardData has no originalRepresentative, no changes should be made"() {
    given: "A message header with ForwardData without originalRepresentative"
    MessageHeader header = new MessageHeader()
    ForwardData forwardData = new ForwardData(
            UUID.randomUUID(),
            LocalDateTime.now(),
            "originalSender",
            "contentResponsible",
            null,
            "contactPointId",
            "comment"
    )
    header.setForwardData(forwardData)

    and: "A converter rule"
    DownConvertHeaderRule rule = new DownConvertHeaderRule()
    Message message = new Message()
    message.setMessageHeader(header)

    when: "Converting the message"
    rule.convert(message)

    then: "No changes should be made to ForwardData"
    MessageHeader convertedHeader = message.getMessageHeader()
    convertedHeader.getForwardData().getOriginalContentResponsible() == "contentResponsible"
    convertedHeader.getForwardData().getOriginalRepresentative() == null
  }

  void "Down conversion: ForwardData conversion with originalRepresentative=#representative and originalContentResponsible=#contentResponsible"() {
    given: "A message header with ForwardData"
    MessageHeader header = new MessageHeader()
    ForwardData forwardData = new ForwardData(
            UUID.randomUUID(),
            LocalDateTime.now(),
            "originalSender",
            contentResponsible,
            representative,
            "contactPointId",
            "comment"
    )
    header.setForwardData(forwardData)

    and: "A converter rule"
    DownConvertHeaderRule rule = new DownConvertHeaderRule()
    Message message = new Message()
    message.setMessageHeader(header)

    when: "Converting the message"
    rule.convert(message)

    then: "Values should be as expected after conversion"
    MessageHeader convertedHeader = message.getMessageHeader()
    convertedHeader.getForwardData().getOriginalContentResponsible() == expectedContentResponsible
    convertedHeader.getForwardData().getOriginalRepresentative() == expectedRepresentative

    where:
    representative | contentResponsible || expectedContentResponsible | expectedRepresentative
     "12345"       | null               || "12345"                    | null
     "12345"       | "existing"         || "12345"                    | null
     null          | "existing"         || "existing"                 | null
     null          | null               || null                       | null
  }
}
