package dk.digst.digital.post.memolib.writer

import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.util.MeMoSchemaTestProvider
import dk.digst.digital.post.memolib.util.MeMoSchemaTestValidator
import spock.lang.Issue
import spock.lang.Shared
import spock.lang.Specification

import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.message

class MeMoStreamWriterImplSpec extends Specification {
  @Shared
  MeMoSchemaTestValidator xmlSchemaValidator
  @Shared
  ByteArrayOutputStream output
  @Shared
  MeMoStreamWriter xmlWriter

  void setup() {
    xmlSchemaValidator = MeMoSchemaTestProvider.getXmlSchemaValidator()
    output = new ByteArrayOutputStream()
    xmlWriter = MeMoWriterFactory.createWriter(output, false, MeMoVersion.MEMO_VERSION_OLD)
  }

  void cleanup() {
    xmlWriter.closeStream()
  }

  @Issue("AC-230")
  void 'Valid XML MeMo message can be generated'() {
    given:
    Message customMessage = message().build()

    when:
    xmlWriter.write(customMessage)

    and:
    xmlWriter.closeStream()

    then:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))
  }

  void 'Valid XML MeMo message of the newest version can be generated'() {
    given:
    Message customMessage = message().build()
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
    MeMoStreamWriter writer = MeMoWriterFactory.createNewWriter(outputStream)

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    when:
    writer.write(customMessage)

    and:
    writer.closeStream()

    then:
    MeMoSchemaTestProvider.getNewXmlSchemaValidator().isValid(new ByteArrayInputStream(outputStream.toByteArray()))
  }

  void 'With validation enabled and valid MeMo message, valid XML MeMo is generated'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoWriterFactory.createWriter(validatingWriterOutput, true, MeMoVersion.MEMO_VERSION_OLD)
    Message customMessage = message().build()

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    noExceptionThrown()

    and:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(validatingWriterOutput.toByteArray()))
  }

  void 'With validation enabled and valid MeMo message, using the new version, a valid XML MeMo is generated'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoWriterFactory.createNewWriter(validatingWriterOutput, true)
    Message customMessage = message().build()

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    noExceptionThrown()

    and:
    MeMoSchemaTestProvider.getNewXmlSchemaValidator().isValid(new ByteArrayInputStream(validatingWriterOutput.toByteArray()))
  }

  void 'With validation enabled and valid MeMo New message, valid XML MeMo is generated'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoWriterFactory.createWriter(validatingWriterOutput, true, MeMoVersion.MEMO_VERSION_OLD)
    Message customMessage = message().build()

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    noExceptionThrown()

  }

  void 'With validation enabled and invalid MeMo message, exception is thrown'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoWriterFactory.createWriter(validatingWriterOutput, true, MeMoVersion.MEMO_VERSION_OLD)

    and:
    Message customMessage = message().build()
    customMessage.messageHeader.messageUUID = null

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    !xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    MeMoWriteException expectedException = thrown(MeMoWriteException)
    expectedException.message.contains("tag name \"messageID\" is not allowed. Possible tag names are: <messageUUID>")
  }
}
