package dk.digst.digital.post.memolib.util

import javax.xml.XMLConstants
import javax.xml.validation.Schema
import javax.xml.validation.SchemaFactory
import lombok.Getter
import org.xml.sax.SAXException

/**
 * The MeMoSchemaProvider can provide a Validator which can validate against the MeMo XML Schema.
 */
@Getter
class MeMoSchemaTestProvider {

  private static final File schemaFile = new File('src/main/resources/schemas/MeMo_core.xsd')

  private static final File schemaNewFile = new File('src/main/resources/schemas/Version1.2/MeMo_core.xsd')

  static MeMoSchemaTestValidator getXmlSchemaValidator() throws SAXException {
    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
    Schema schema = schemaFactory.newSchema(schemaFile)
    return new MeMoSchemaTestValidator(schema.newValidator())
  }

  static MeMoSchemaTestValidator getNewXmlSchemaValidator() throws SAXException {
    SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
    Schema schema = schemaFactory.newSchema(schemaNewFile)
    return new MeMoSchemaTestValidator(schema.newValidator())
  }
}

