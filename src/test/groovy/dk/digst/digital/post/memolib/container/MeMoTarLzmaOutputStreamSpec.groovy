package dk.digst.digital.post.memolib.container

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.util.stream.Collectors
import org.apache.commons.compress.archivers.tar.TarConstants
import org.apache.commons.compress.archivers.zip.NioZipEncoding
import org.apache.commons.compress.archivers.zip.ZipEncoding
import org.apache.commons.lang3.StringUtils
import spock.lang.Shared
import spock.lang.Specification

class MeMoTarLzmaOutputStreamSpec extends Specification {

  public static final String MEMO1 = StringUtils.repeat('memo-', 40) + '/"' + StringUtils.repeat('name-', 40) + 'memo1.xml'

  public static final String CHINEESE = '信息'

  public static final String JAPANEESE = 'メッセージ'

  public static final String KOREEAN = '메시지'

  public static final String THAI = 'ข้อความ'

  public static final String HEBREW = 'הוֹדָעָה'

  public static final String POLISH = 'wiadomość'
  
  public static final String RUSSIAN = 'сообщение'

  public static final String PUNJABI = 'ਸੁਨੇਹਾ'

  public static final String ARABIC = 'رسالة'

  public static final String MEMO2 = createFilename(CHINEESE, JAPANEESE, KOREEAN, THAI, HEBREW, POLISH, PUNJABI, ARABIC)

  @Shared private ByteArrayOutputStream outputStream

  @Shared private TarLzmaContainerOutputStream memoTarLzma

  void setup() {
    outputStream = Spy(ByteArrayOutputStream)
    memoTarLzma = new TarLzmaContainerOutputStream(outputStream)
  }

  void cleanup() {
    memoTarLzma.close()
  }

  void 'do not accept null as input for constructor'() {
    when:
    new TarLzmaContainerOutputStream(null)

    then:
    thrown NullPointerException
  }

  void 'writeNextEntry returns a ByteArrayOutputStream'() {
    when:
    OutputStream entry = memoTarLzma.writeNextEntry(MEMO1)

    then:
    entry instanceof TarEntryOutputStream
  }

  void 'two entries can be written to the MeMoZipOutputStream'() {
    when:
    memoTarLzma.writeNextEntry(MEMO1)
    memoTarLzma.closeEntry()
    memoTarLzma.writeNextEntry(MEMO2)
    memoTarLzma.closeEntry()
    memoTarLzma.close()

    then:
    ArchiveEntryIterator entryIterator = getOutputEntries()

    and: 'first entry has been written'
    entryIterator.hasEntry()
    entryIterator.getEntry().key == MEMO1

    and: 'second entry has been written'
    entryIterator.hasEntry()
    entryIterator.nextEntry()
    entryIterator.getEntry().key == MEMO2
  }

  void 'not closing entry correctly causes an exception to be thrown'() {
    when:
    memoTarLzma.writeNextEntry(MEMO1)
    memoTarLzma.writeNextEntry(MEMO2)

    then:
    thrown IllegalStateException
  }

  void 'writing to an entry after it has been closed causes a IllegalStateException to be thrown'() {
    when: 'write entry and close it'
    OutputStream entry = memoTarLzma.writeNextEntry(MEMO1)
    memoTarLzma.closeEntry()

    and: 'try and add content to the entry'
    entry.write(1)

    then:
    thrown IllegalStateException
  }

  void 'close should also close the underlying output stream'() {
    when: 'write first entry'
    memoTarLzma.close()

    then:
    1 * outputStream.close()
  }

  void 'trying to write a new entry to the stream after it has been closed causes a IOException to be thrown'() {
    when: 'write first entry'
    memoTarLzma.writeNextEntry(MEMO1)

    and: 'close the MeMo zip output stream'
    memoTarLzma.close()

    and: 'try and write a new entry'
    memoTarLzma.writeNextEntry(MEMO2)

    then:
    IOException expectedException = thrown(IOException)
    expectedException.message == 'Stream has already been finished'
  }

  ArchiveEntryIterator getOutputEntries() {
    InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray())
    new ArchiveEntryIterator(new AutoDetectArchiveInputStreamFactory().newArchiveInputStream(inputStream))
  }
  
  private static String createFilename(String... names) {
    List<String> filenames = []
    
    int maxLen = TarConstants.NAMELEN - 4
    int nameMaxLen = maxLen / names.size()
    int nameLenSum = 0

    ZipEncoding encoder = new NioZipEncoding(StandardCharsets.UTF_8, false)
    
    int count = 1
    return Arrays.stream(names)
      .map { name -> 
        ByteBuffer buffer = encoder.encode(name)
        int nameLen = buffer.limit() - buffer.position()
        int thisNameMaxLen = (nameMaxLen * count - nameLenSum)
        if (nameLen > thisNameMaxLen) {
          int pos = name.length() - 1
          while (nameLen > thisNameMaxLen) {
            name = name.substring(0, pos)
            buffer = encoder.encode(name)
            nameLen = buffer.limit() - buffer.position()
            pos--
          }
        }
        nameLenSum += nameLen
        count++
        return name
      }.collect(Collectors.joining(''))
  }
}
