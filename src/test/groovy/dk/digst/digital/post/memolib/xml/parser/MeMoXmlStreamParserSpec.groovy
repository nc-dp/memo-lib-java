package dk.digst.digital.post.memolib.xml.parser

import dk.digst.digital.post.memolib.builder.FileBuilder
import dk.digst.digital.post.memolib.builder.FileContentBuilder
import dk.digst.digital.post.memolib.builder.MainDocumentBuilder
import dk.digst.digital.post.memolib.builder.MessageBodyBuilder
import dk.digst.digital.post.memolib.builder.MessageBuilder
import dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory
import dk.digst.digital.post.memolib.model.Action
import dk.digst.digital.post.memolib.model.AdditionalDocument
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.FileContent
import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.MeMoClass
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.parser.MeMoParseException
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.memolib.util.MeMoTestDataProvider
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import dk.digst.digital.post.memolib.writer.MeMoWriterFactory
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamProcessorVisitor
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitor
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitorFactory
import java.time.LocalDateTime
import spock.lang.Shared
import spock.lang.Specification

class MeMoXmlStreamParserSpec extends Specification {

  @Shared private MeMoParser parser

  @Shared private FileInputStream fileInputStream

  void setup() {
    java.io.File memo = new java.io.File("src/test/resources/xml/MeMo_Full_Example.xml")
    fileInputStream = new FileInputStream(memo)
    parser = MeMoParserFactory.createParser(fileInputStream, true)
  }

  void 'the parser can traverse the whole MeMo '() {
    expect:
    !parser.isClosed()

    when: "the MeMo is traversed"
    parser.traverse()

    then: "the parser has been closed"
    parser.isClosed()

    and: "the input stream is still open"
    fileInputStream.available() == 0
    noExceptionThrown()
  }

  void 'the parser can parse the whole MeMo '() {
    expect:
    !parser.isClosed()

    when: "the MeMo is traversed"
    parser.parse()

    then: "the parser has been closed"
    parser.isClosed()

    and: "the input stream is still open"
    fileInputStream.available() == 0
    noExceptionThrown()
  }

  void 'If XML Schema validation is enabled, the parser throws an exception if the xml is not valid'() {
    given: "an invalid MeMo xml message"
    java.io.File memo = new java.io.File("src/test/resources/xml/MeMo_Full_Example_Invalid.xml")
    fileInputStream = new FileInputStream(memo)
    parser = MeMoParserFactory.createParser(fileInputStream, true)

    when: "the MeMo is traversed"
    parser.traverse()

    then: "the xml stream is closed"
    MeMoParseException expectedException = thrown(MeMoParseException)
    expectedException.message.contains("the value is not a member of the enumeration")
    parser.isClosed()
  }

  void 'The parser can parse the MeMo class elements for a message with an attached file'() {
    given: "a message with a main document containing a pdf file"
    FileContent fileContent = FileContentBuilder.newBuilder()
            .location(MeMoTestDataProvider.modelPdf())
            .build()

    File file = FileBuilder.newBuilder()
            .encodingFormat("application/pdf")
            .filename("model.pdf")
            .inLanguage("da")
            .content(fileContent)
            .build()

    MainDocument mainDocument = MainDocumentBuilder.newBuilder()
            .mainDocumentId("19387473733")
            .label("brev fra kommunen")
            .addFile(file)
            .build()

    MessageBody messageBody = MessageBodyBuilder.newBuilder()
            .createdDateTime(LocalDateTime.parse("2100-05-03T12:00:00"))
            .mainDocument(mainDocument)
            .build()

    Message message = MessageBuilder.newBuilder()
            .messageHeader(PrefilledMessageBuilderFactory.messageHeader().build())
            .messageBody(messageBody)
            .build()

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream()

    and: "the message is serialized"
    MeMoStreamWriter writer = MeMoWriterFactory.createWriter(outputStream)
    writer.write(message)
    writer.close()
    ByteArrayInputStream memoInputStream = new ByteArrayInputStream(outputStream.toByteArray())

    and: "a validating parser is created"
    parser = MeMoParserFactory.createParser(memoInputStream, true)

    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(MeMoClass.class,
                                                                        { cursor -> cursor.parseMeMoClassElements() })

    when: "the MeMo is traversed with a visiting parsing the elements"
    parser.traverse(visitor)

    then: "the MeMo is traversed without any exception being thrown"
    noExceptionThrown()
  }

  void 'A visitor can visit a MeMo message class when the parser traverses the MeMo'() {
    given: "a visitor which collects the name of classes visited"
    Set<String> visitedClasses = new HashSet<>()
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(MeMoClass.class,
                                                                        { cursor -> visitedClasses.add(cursor.getCurrentPosition().getSimpleName()) })

    when: "the MeMo is traversed"
    parser.traverse(visitor)

    then: "All MeMo message classes has been visited"
    visitedClasses.sort() == ['Action', 'AdditionalContentData', 'AdditionalDocument', 'AdditionalReplyData', 'Address',
                              'AddressPoint', 'AttentionData', 'AttentionPerson', 'CaseId', 'ContactInfo', 'ContactPoint',
                              'ContentData', 'ContentResponsible', 'CprData', 'CvrData', 'Education', 'EidData', 'Email',
                              'EntryPoint', 'File', 'FormData', 'ForwardData', 'GeneratingSystem', 'GlobalLocationNumber',
                              'KleData', 'MainDocument', 'Message', 'MessageBody', 'MessageHeader', 'MotorVehicle',
                              'ProductionUnit', 'PropertyNumber', 'Recipient', 'ReplyData', 'Reservation',
                              'SeNumber', 'Sender', 'SorData', 'TechnicalDocument', 'Telephone', 'UnstructuredAddress']
  }

  void 'A visitor can parse certain element of the MeMo message'() {
    given: "a visitor which parses all Files"
    List<File> files = []
    MeMoStreamVisitor fileVisitor = MeMoStreamVisitorFactory.createConsumer(
            File, { cursor -> files.add(cursor.parseMeMoClass()) })

    when: "the MeMo is traversed"
    parser.traverse(fileVisitor)

    then: "All files has been visited and parsed"
    files.size() == 6
    files.collect({ it -> it.filename }) == ['Pladsanvisning.pdf', 'Pladsanvisning.txt', 'Pladsanvisning.pdf',
                                             'Praktiske oplysninger.doc', 'vejledning.pdf', 'TekniskDokument.xml']
  }

  void 'A visitor can get fields for a certain class in the MeMo message'() {
    given: "a visitor which parses all Files"
    List<String> additionalDocumentIds = []
    MeMoStreamVisitor fileVisitor = MeMoStreamVisitorFactory.createConsumer(AdditionalDocument,
                                                                            { cursor ->
                                                                              MeMoClassElements elements = cursor.parseMeMoClassElements()
                                                                              additionalDocumentIds.add(elements.get("additionalDocumentID").orElseThrow())
                                                                            })

    when: "the MeMo is traversed"
    parser.traverse(fileVisitor)

    then: "All files has been visited and parsed"
    additionalDocumentIds == ['789', '678']
  }

  void 'A visitor can get optional elements for a certain class in the MeMo message'() {
    given: "a visitor which parses all Files"
    List<LocalDateTime> actionStartDateTimes = []
    int numberOfActions = 0
    MeMoStreamVisitor actionStartDateTimeVisitor = MeMoStreamVisitorFactory.createConsumer(Action,
                                                                                           { cursor ->
                                                                                             MeMoClassElements elements = cursor.parseMeMoClassElements()
                                                                                             elements.get("startDateTime", LocalDateTime).ifPresent({ startDateTime -> actionStartDateTimes.add(startDateTime) })
                                                                                           })

    MeMoStreamVisitor actionVisitor = MeMoStreamVisitorFactory.createConsumer(Action, { cursor -> numberOfActions++ })

    when: "the MeMo is traversed"
    parser.traverse(actionStartDateTimeVisitor, actionVisitor)

    then: "All actions has been visited and parsed"
    numberOfActions == 5

    and: "Only the four actions which has a startDateTime has been added to the list"
    actionStartDateTimes == [LocalDateTime.of(2018, 11, 9, 12, 0),
                             LocalDateTime.of(2018, 11, 10, 9, 0),
                             LocalDateTime.of(2018, 11, 10, 9, 0),
                             LocalDateTime.of(2018, 11, 10, 9, 0)]
  }

  void 'Trying to parse MeMo class and elements causes the parser to throw an exception'() {
    given: "a visitor which parses all Files"
    List<String> additionalDocumentIds = []
    MeMoStreamVisitor fileVisitor = MeMoStreamVisitorFactory.createConsumer(AdditionalDocument,
                                                                            { cursor ->
                                                                              MeMoClassElements elements = cursor.parseMeMoClassElements()
                                                                              AdditionalDocument additionalDocument = cursor.parseMeMoClass()
                                                                            })

    when: "the MeMo is traversed"
    parser.traverse(fileVisitor)

    then: "An exception is thrown"
    IllegalStateException expected = thrown(IllegalStateException)
    expected.message == "MeMo Class can't be parsed. parseElements has already been called"
  }

  void 'A visitor can collect certain element of the MeMo message'() {
    given: "a visitor which collects all AdditionalDocuments"
    MeMoStreamProcessorVisitor<AdditionalDocument> visitor = MeMoStreamVisitorFactory.createProcessor(AdditionalDocument)

    when: "the MeMo is traversed"
    parser.traverse(visitor)
    List<AdditionalDocument> additionalDocuments = visitor.getResult()

    then: "All files has been visited and parsed"
    additionalDocuments.size() == 2
    additionalDocuments.collect({ it -> it.additionalDocumentId }) == ['789', '678']
  }

  void 'A visitor can traverse the children of a MeMO class'() {
    given: "a visitor which collects the file names"
    List<String> filenames = []
    MeMoStreamVisitor fileVisitor = MeMoStreamVisitorFactory.createConsumer(File,
                                                                            { fileCursor ->
                                                                              MeMoClassElements elements = fileCursor.parseMeMoClassElements()
                                                                              filenames.add(elements.get("filename").orElseThrow())
                                                                            })

    and: "a visitor which consumes the additional documents"
    List<String> additionalDocumentIds = []
    MeMoStreamVisitor documentVisitor = MeMoStreamVisitorFactory.createConsumer(AdditionalDocument,
                                                                                { documentCursor ->
                                                                                  MeMoClassElements elements = documentCursor.parseMeMoClassElements()
                                                                                  additionalDocumentIds.add(elements.get("additionalDocumentID").orElseThrow())

                                                                                  documentCursor.traverseChildren(fileVisitor)
                                                                                })

    when: "the MeMo is traversed"
    parser.traverse(documentVisitor)

    then: "All additional documents with files has been visited"
    additionalDocumentIds == ['789', '678']
    filenames == ['Pladsanvisning.pdf', 'Praktiske oplysninger.doc', 'vejledning.pdf']
  }

  void 'Calling the close method only closes the xml stream'() {
    expect: "the file input stream is open"
    fileInputStream.available()
    !parser.isClosed()

    when: "the close method is called"
    parser.close()

    then: "the file input stream is still available"
    fileInputStream.available()

    and: "the parser is closed"
    parser.isClosed()
  }

  void 'Calling the close method twice does not cause an exception'() {
    expect: "the file input stream is open"
    fileInputStream.available()
    !parser.isClosed()

    when: "the close method is called"
    parser.close()
    parser.close()

    then: "no exception is thrown"
    noExceptionThrown()
  }

  void 'Calling the close method after traverse does not cause an exception'() {
    when: "the MeMo is traversed"
    parser.parse()
    parser.close()

    then: "no exception is thrown"
    noExceptionThrown()
  }
}
