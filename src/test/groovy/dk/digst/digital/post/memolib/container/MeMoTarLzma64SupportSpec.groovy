package dk.digst.digital.post.memolib.container

import dk.digst.digital.post.memolib.builder.AdditionalDocumentBuilder
import dk.digst.digital.post.memolib.builder.FileBuilder
import dk.digst.digital.post.memolib.builder.FileContentBuilder
import dk.digst.digital.post.memolib.builder.MessageBuilder
import dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory
import dk.digst.digital.post.memolib.model.AdditionalDocument
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.FileContent
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.util.MeMoTestDataProvider
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
@Ignore("This Specification takes 5-6 minutes to execute, so the features are disabled by default")
class MeMoTarLzma64SupportSpec extends Specification {

  private static final NUMBER_OF_MEMOS = 100000

  @Shared private java.io.File tempFile

  void setupSpec() {
    tempFile = java.io.File.createTempFile("temp", ".tar.lzma")
    println "created temporary file: ${tempFile.getAbsolutePath()}"
    tempFile.deleteOnExit()
  }

  void 'write a lzma file with a total size of 4,5 GB containing 100000 MeMo messages'() {
    given: "a memo container writer"
    ContainerOutputStream meMoContainer = new TarLzmaContainerOutputStream(new FileOutputStream(tempFile))
    MeMoContainerWriter containerWriter = new MeMoContainerWriter(meMoContainer)

    and: "a message"
    FileContent fileContent = FileContentBuilder.newBuilder()
            .location(MeMoTestDataProvider.notatPdf())
            .build()

    File file = FileBuilder.newBuilder()
            .encodingFormat("application/pdf")
            .filename("model.pdf")
            .inLanguage("da")
            .content(fileContent)
            .build()

    AdditionalDocument additionalDocument = AdditionalDocumentBuilder.newBuilder()
            .additionalDocumentId(UUID.randomUUID().toString())
            .label("model")
            .addFile(file)
            .build()

    MessageBody messageBody = PrefilledMessageBuilderFactory.messageBody()
            .addAdditionalDocument(additionalDocument)
            .build()

    Message message = MessageBuilder.newBuilder()
            .messageHeader(PrefilledMessageBuilderFactory.messageHeader().build())
            .messageBody(messageBody)
            .build()

    when: "the message are written to the tar file"
    NUMBER_OF_MEMOS.times { idx ->
      containerWriter.writeEntry("${idx.toString()}.xml", message)
/*
      if (idx % 1000 == 0) {
        println "${idx} done"
      }
*/
    }
    then: "the file has been created"
    containerWriter.close()
    tempFile.exists()

    cleanup:
    meMoContainer.close()
    containerWriter.close()
  }

  void "Read a zip file with a total size of 4,5 GB containing 100000 MeMo messages"() {
    given: "a memo container reader"
    MeMoContainerReader containerReader = new MeMoContainerReaderFactory().newContainerReader(
            new FileInputStream(tempFile)) as MeMoContainerReader

    when: "the zip file is read"
    int filesRead = 0
    while (containerReader.hasEntry()) {
      containerReader.readEntry().orElseThrow()
      filesRead++
/*
      if (filesRead % 10000 == 0) {
        println "${filesRead} done"
      }
*/
    }

    and: "the zip file is closed"
    containerReader.close()

    then: "all entries could be read"
    noExceptionThrown()
    filesRead == NUMBER_OF_MEMOS

    cleanup:
    containerReader.close()
  }
}
