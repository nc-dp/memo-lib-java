package dk.digst.digital.post.memolib.xml.parser

import dk.digst.digital.post.memolib.model.File

import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.xml.stax.Stax2FactoryProvider
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitor
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitorFactory
import spock.lang.Shared
import spock.lang.Specification

class MeMoXmlStreamCursorSpec extends Specification {

  @Shared MeMoXmlStreamParser parser

  void setup() {
    parser = Mock(MeMoXmlStreamParser, constructorArgs: [null, new ByteArrayInputStream(), Stax2FactoryProvider.createXMLStreamReaderFactory(false)])
  }

  void cleanup() {
    parser.close()
  }

  void "the cursor can parse the elements of a MeMo class"() {
    given: "a cursor at main document file"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    expect:
    cursor.getState() == MeMoXmlStreamCursor.CursorState.INITIAL_POSITION

    when: "the elements are parsed"
    MeMoClassElements parsedElements = cursor.parseMeMoClassElements()

    then: "the elements has been parsed and the cursor state has been updated"
    parsedElements == fileElements
    cursor.getState() == MeMoXmlStreamCursor.CursorState.ELEMENTS_PARSED
  }

  void "calling the method to parse the elements of a MeMo class twice returns the same result"() {
    given: "a cursor at main document file"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    when: "the elements are parsed"
    MeMoClassElements parsedElements = cursor.parseMeMoClassElements()
    MeMoClassElements parsedElements2 = cursor.parseMeMoClassElements()

    then: "the elements has been parsed and both invocations has returned the same result"
    parsedElements == fileElements
    parsedElements2 == fileElements
  }

  void "the cursor can parse the MeMo class"() {
    given: "a cursor at main document file"
    File file = new File()
    parser.parseClass(_) >> file

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    expect:
    cursor.getState() == MeMoXmlStreamCursor.CursorState.INITIAL_POSITION

    when: "the MeMo class is parsed"
    File parsedFile = cursor.parseMeMoClass()

    then: "the elements has been parsed and the cursor state has been updated"
    parsedFile == file
    cursor.getState() == MeMoXmlStreamCursor.CursorState.PARSED
  }

  void "calling the method to parse MeMo class twice returns the same result"() {
    given: "a cursor at main document file"
    File file = new File()
    parser.parseClass(_) >> file

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    when: "the MeMo class is parsed"
    File parsedFile = cursor.parseMeMoClass()
    File parsedFile2 = cursor.parseMeMoClass()

    then: "the elements has been parsed and both invocations has returned the same result"
    parsedFile == file
    parsedFile2 == file
  }

  void "the cursor is not able to parse the elements after the MeMo class has been parsed"() {
    given: "a cursor at main document file"
    parser.parseClass(_) >> new File()
    parser.parseElements() >> new MeMoClassElements()

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    when: "the MeMo class is parsed"
    File parsedFile = cursor.parseMeMoClass()

    and: "the elements are parsed"
    cursor.parseMeMoClassElements()

    then: "the MeMo class has been parsed, but the subsequent call to parse the elements throws an exception"
    IllegalStateException exception = thrown(IllegalStateException)
    exception.message == "MeMo Class elements can't be parsed. parseMeMoClass has already been called"
    parsedFile != null
  }

  void "the cursor is not able to parse the MeMo class after the elements has been parsed"() {
    given: "a cursor at main document file"
    parser.parseClass(_) >> new File()
    parser.parseElements() >> new MeMoClassElements()

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    and: "the elements are parsed"
    MeMoClassElements parseElements = cursor.parseMeMoClassElements()

    when: "the MeMo class is parsed"
    cursor.parseMeMoClass()

    then: "the MeMo class has been parsed, but the subsequent call to parse the elements throws an exception"
    IllegalStateException exception = thrown(IllegalStateException)
    exception.message == "MeMo Class can't be parsed. parseElements has already been called"
    parseElements != null
  }

  void "the cursor can accept a visitor"() {
    given: "a cursor at main document file"
    File file = new File()
    parser.parseClass(_) >> file

    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)
    File parsedFile

    and: "a visitor"
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File, { fileCursor -> parsedFile = fileCursor.parseMeMoClass() })

    when: "a visitor is accepted"
    cursor.accept(visitor)

    then: "the visitMeMoClass method of the visitor has been called"
    parsedFile == file
  }

  void "the cursor can use the parser to traverse the child MeMo classes"() {
    given: "a cursor at main document"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("MainDocument", location, parser)
    File parsedFile

    and: "a visitor"
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File, { fileCursor -> parsedFile = fileCursor.parseMeMoClass() })

    when: "a visitor is accepted"
    cursor.traverseChildren(visitor)

    then: "the parser has been called with the expected parameters"
    1 * parser.traverseChildren({
      assert location.currentPosition == MainDocument.class
      assert location.depth == 3
      assert location.parentPosition.orElseThrow() == MessageBody.class
    }, 3, visitor) >> {}
  }

  void "the cursor can return the xml element name of the current position"() {
    given: "a cursor at message"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("Message", location, parser)

    when: "the name of the xml element is requested"
    String xmlElementName = cursor.getXmlElementName()

    then: "Message was returned"
    xmlElementName == "Message"
  }

  void "the cursor can return the MeMo class of the current position"() {
    given: "a cursor at main document"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("MainDocument", location, parser)

    when: "the parent position is requested"
    Class current = cursor.getCurrentPosition()

    then: "the parent MeMo class is MessageBody"
    current == MainDocument.class
  }

  void "the cursor can return the parent of the current position"() {
    given: "a cursor at main document"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("MainDocument", location, parser)

    when: "the parent position is requested"
    Class parent = cursor.getParentPosition().orElseThrow()

    then: "the parent MeMo class is MessageBody"
    parent == MessageBody.class
  }

  void "the cursor returns Optional.empty if there is not parent to the current position"() {
    given: "a cursor at message"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("Message", location, parser)

    when: "the parent position is requested"
    Optional<Class> parent = cursor.getParentPosition()

    then: "there is no parent"
    !parent.isPresent()
  }
}
