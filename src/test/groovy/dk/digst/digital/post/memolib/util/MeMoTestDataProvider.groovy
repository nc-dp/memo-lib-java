package dk.digst.digital.post.memolib.util

/**
 * Used for testing.
 */
class MeMoTestDataProvider {

  static String memo_archive_2_messages = 'memo_archive_2_messages'

  static String memo_archive_2_messages_too_large_dict = 'memo_archive_2_messages_too_large_dict'

  static String memo_container_from_command_line = 'memo_container_from_command_line'

  static String memo_container_7z_windows_from_command_line = 'memo_container_7z_windows_from_command_line'
  
  static String memo_archive_1_invalid_message = 'memo_archive_1_invalid_message'

  static String memo_archive_0_messages = 'memo_archive_0_messages'

  static FileInputStream streamMeMoContainerZipWithTwoMessages() throws FileNotFoundException {
    return createZipFileInputStream(memo_archive_2_messages)
  }

  static FileInputStream streamMeMoContainerTarLzmaWithTwoMessages() throws FileNotFoundException {
    return createTarLzmaFileInputStream(memo_archive_2_messages)
  }

  static FileInputStream streamMeMoContainerTarLzmaFromCommandLine() throws FileNotFoundException {
    return createTarLzmaFileInputStream(memo_container_from_command_line)
  }

  static FileInputStream streamMeMoContainerTarLzma7zWindowsFromCommandLine() throws FileNotFoundException {
    return createTarLzmaFileInputStream(memo_container_7z_windows_from_command_line)
  }

  static FileInputStream streamMeMoContainerTarLzmaWithTwoMessagesTooLargeDict() throws FileNotFoundException {
    return createTarLzmaFileInputStream(memo_archive_2_messages_too_large_dict)
  }

  static FileInputStream streamMeMoContainerZipWithOneInvalidMessage() throws FileNotFoundException {
    return createZipFileInputStream(memo_archive_1_invalid_message)
  }

  static FileInputStream streamMeMoContainerZipWithZeroMessages() throws FileNotFoundException {
    return createZipFileInputStream(memo_archive_0_messages)
  }

  static FileInputStream streamMeMoMessageMinimumExample() throws FileNotFoundException {
    return createXmlFileInputStream('MeMo_Minimum_Example')
  }

  static FileInputStream streamMeMoMessageFullExample() throws FileNotFoundException {
    return createXmlFileInputStream('MeMo_Full_Example')
  }

  static FileInputStream streamInvalidMeMoMessage() throws FileNotFoundException {
    return createXmlFileInputStream('MeMo_Full_Example_Invalid')
  }

  static String modelPdf() throws FileNotFoundException {
    return 'src/test/resources/files/model.pdf'
  }

  static String notatPdf() throws FileNotFoundException {
    return 'src/test/resources/files/notat.pdf'
  }

  static FileInputStream createZipFileInputStream(String filename) {
    return new FileInputStream(zipFile(filename))
  }

  static FileInputStream createTarLzmaFileInputStream(String filename) {
    return new FileInputStream(tarLzmaFile(filename))
  }

  static FileInputStream createXmlFileInputStream(String filename) {
    return new FileInputStream("src/test/resources/xml/${filename}.xml")
  }

  static String zipFile(String filename) {
    return "src/test/resources/files/${filename}.zip"
  }

  static String tarLzmaFile(String filename) {
    return "src/test/resources/files/${filename}.tar.lzma"
  }
}
