package dk.digst.digital.post.memolib.xml.writer

import dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory
import dk.digst.digital.post.memolib.model.AdditionalDocument
import dk.digst.digital.post.memolib.model.ExternalFileContent
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.util.ResultParser
import spock.lang.Shared
import spock.lang.Specification

class AdditionalDocumentWriterSpec extends Specification {

  @Shared
  MeMoStreamWriterImpl meMoStreamWriter

  @Shared
  ByteArrayOutputStream output

  void setup() {
    output = new ByteArrayOutputStream()
    meMoStreamWriter = MeMoXmlWriterFactory.createWriter(output)
  }

  void cleanup() {
    meMoStreamWriter.closeStream()
  }

  def 'write digital document including containing one action and one file'() {
    given:
    AdditionalDocumentWriter writer = meMoStreamWriter.writeStartOfMessage(
            new Message(memoVersion: MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber(),
                        memoSchVersion: MeMoVersion.MEMO_SCH_VERSION))
            .body()
            .additionalDocumentWriter()

    AdditionalDocument digitalDocument = PrefilledMessageBuilderFactory
            .additionalDocument().build()

    when: 'write the digital document to the MeMo message'
    writer.write(digitalDocument)

    and: 'close and flush the writer'
    meMoStreamWriter.closeStream()

    then:
    Message message = ResultParser.parseOutputMessage(output)
    message.messageBody.additionalDocument.size() == 1

    assertDigitalDocument(message.messageBody.additionalDocument[0], 1)
    assertFile(message.messageBody.additionalDocument[0].file[0], 4)
  }

  def 'write digital document including a file, provided as an input stream'() {
    given:
    AdditionalDocumentWriter writer = meMoStreamWriter.writeStartOfMessage(
            new Message(memoVersion: MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber(),
                        memoSchVersion: MeMoVersion.MEMO_SCH_VERSION))

            .body()
            .additionalDocumentWriter()

    String filePath = 'src/test/resources/files/model.pdf'
    File memoFile = PrefilledMessageBuilderFactory.file()
            .content(new ExternalFileContent(filePath, false))
            .build()

    AdditionalDocument digitalDocument = PrefilledMessageBuilderFactory.additionalDocument()
            .file([memoFile])
            .build()

    when: 'write the digital document to the MeMo message'
    writer.write(digitalDocument)

    and: 'close and flush the writer'
    meMoStreamWriter.closeStream()

    then:
    Message message = ResultParser.parseOutputMessage(output)
    message.messageBody.additionalDocument.size() == 1
    assertCorrectBase64Encoding(message.messageBody.additionalDocument[0].file[0], new java.io.File(filePath))
  }

  void assertDigitalDocument(AdditionalDocument digitalDocument, int actionCount) {
    assert digitalDocument.label == 'someDigitalDocumentLabel & another'
    assert digitalDocument.additionalDocumentId == 'someAdditionalDocumentID'

    assert actionCount != 0 ? digitalDocument.action.size() == actionCount : digitalDocument.action == null
  }

  void assertFile(File file, int length) {
    assert file.encodingFormat == 'application/pdf'
    assert file.filename == 'model.pdf'
    assert file.language == 'da'
    assert file.content.streamContent().getBytes().length == length
  }

  void assertCorrectBase64Encoding(File file, java.io.File originalFile) {
    assert file.encodingFormat == 'application/pdf'
    assert file.filename == 'model.pdf'
    assert file.language == 'da'

    byte[] decodedContent = Base64.decoder.decode(file.content.streamContent().getBytes())
    assert Arrays.equals(originalFile.readBytes(), decodedContent)
  }
}
