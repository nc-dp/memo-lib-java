package dk.digst.digital.post.memolib.util;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.memolib.parser.MeMoParser;
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParserFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;

public class ResultParser {

  public static Message parseOutputMessage(ByteArrayOutputStream output)
      throws IOException, MeMoParseException, XMLStreamException {

    MeMoParser parser =
        MeMoXmlStreamParserFactory.createParser(
            new ByteArrayInputStream(output.toByteArray()));
    return parser.parse();
  }
}
