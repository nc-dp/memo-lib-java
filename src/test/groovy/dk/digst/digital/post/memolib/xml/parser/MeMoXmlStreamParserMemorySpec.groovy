package dk.digst.digital.post.memolib.xml.parser

import dk.digst.digital.post.memolib.builder.FileBuilder
import dk.digst.digital.post.memolib.builder.FileContentBuilder
import dk.digst.digital.post.memolib.builder.MainDocumentBuilder
import dk.digst.digital.post.memolib.builder.MessageBodyBuilder
import dk.digst.digital.post.memolib.builder.MessageBuilder
import dk.digst.digital.post.memolib.builder.MessageHeaderBuilder
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitor
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitorFactory
import dk.digst.digital.post.memolib.xml.writer.MeMoXmlWriterFactory
import spock.lang.Shared
import spock.lang.Specification

class MeMoXmlStreamParserMemorySpec extends Specification {

  @Shared private MeMoParser parser

  @Shared private FileInputStream fileInputStream

  private static String oneMbFilePath = "src/test/resources/files/1mbfile.xml"

  @Shared
  private String path = "build/tmp/largeFile.xml"

  void setupSpec() {
    java.io.File file = new java.io.File(path)
    Message message = build100MbMeMo()
    OutputStream stream = new FileOutputStream(file)
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(stream)
    writer.write(message)
    writer.close()
    stream.close()
  }

  void setup(){
    fileInputStream = new FileInputStream(path)
    parser = MeMoParserFactory.createParser(fileInputStream)
  }

  void 'the parser can parse the whole MeMo'() {
    expect:
    !parser.isClosed()

    when: "the MeMo is parsed"
    long initialMemoryUsage = getCurrentMemoryUsageInMb()
    Message message = parser.parse()

    then: "the memory is increased at least 100MB (100 files of >1 MB)"
    (getCurrentMemoryUsageInMb() - initialMemoryUsage) >= 100
    message
  }

  void 'A visitor can parse File elements of the MeMo message and memory does not grow out of control'() {
    given: "a visitor which parses File"
    long initialMemoryUsage = getCurrentMemoryUsageInMb()
    MeMoStreamVisitor memoryRecorderVisitor = MeMoStreamVisitorFactory.createConsumer(File,
            { cursor ->
              File file = cursor.parseMeMoClass()
              assert file
              assert (getCurrentMemoryUsageInMb() - initialMemoryUsage) < 15
            })

    when: "the MeMo is traversed"
    parser.traverse(memoryRecorderVisitor)

    then: "Memory has not grown out of control"
    (getCurrentMemoryUsageInMb() - initialMemoryUsage) < 5
  }

  private static long getCurrentMemoryUsageInMb() {
    Runtime runtime = Runtime.getRuntime()
    runtime.gc()
    return (runtime.totalMemory() - runtime.freeMemory()) / (1024**2)
  }

  private static Message build100MbMeMo() {
    MessageBuilder builder = MessageBuilder.newBuilder().messageHeader(MessageHeaderBuilder.newBuilder().build())

    List<File> files = new ArrayList<>()
    for (int i = 0; i < 100; i++) {
      files.add(FileBuilder.newBuilder().content(
              FileContentBuilder.newBuilder().location(oneMbFilePath).build()).build())
    }
    return builder.messageBody(MessageBodyBuilder.newBuilder().mainDocument(
            MainDocumentBuilder.newBuilder().file(files).build()).build()).build()
  }
}
