package dk.digst.digital.post.memolib.xml.parser

import dk.digst.digital.post.memolib.parser.MeMoParser
import spock.lang.Specification

class MeMoXmlStreamParserFactorySpec extends Specification {

  def "a non-validating parser can be created"() {
    when:
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(new ByteArrayInputStream())

    then:
    parser != null
  }

  def "a validating parser can be created"() {
    when:
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(new ByteArrayInputStream(), true)

    then:
    parser != null
  }
}
