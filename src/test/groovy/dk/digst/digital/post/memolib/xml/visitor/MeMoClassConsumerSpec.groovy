package dk.digst.digital.post.memolib.xml.visitor


import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.model.TechnicalDocument
import dk.digst.digital.post.memolib.xml.parser.MeMoClassElements
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamCursor
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamLocation
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParser
import dk.digst.digital.post.memolib.xml.stax.Stax2FactoryProvider
import spock.lang.Shared
import spock.lang.Specification

class MeMoClassConsumerSpec extends Specification {

  @Shared MeMoXmlStreamParser parser

  void setup() {
    parser = Mock(MeMoXmlStreamParser, constructorArgs: [null, new ByteArrayInputStream(), Stax2FactoryProvider.createXMLStreamReaderFactory(false)])
  }

  void cleanup() {
    parser.close()
  }

  void "the consumer can visit a MeMo class"() {
    given: "a cursor at a file in main document"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    and: "a visitor"
    MeMoClassElements parsedElements
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File.class,
            { c -> parsedElements = c.parseMeMoClassElements() })

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has been consumed and handled by the expression"
    parsedElements == fileElements
  }

  void "the consumer will not consume another MeMo class than the one specified"() {
    given: "a cursor at a technical document"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("TechnicalDocument", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument), parser)

    and: "a visitor"
    MeMoClassElements parsedElements
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File.class,
            { c -> parsedElements = c.parseMeMoClassElements() })

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has not been consumed"
    parsedElements == null
  }

  void "the consumer can visit a MeMo class and consume it if the parent MeMo class is the same as the one specified"() {
    given: "a cursor at a file in main document"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    and: "a visitor"
    MeMoClassElements parsedElements
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File.class, MainDocument.class,
            { c -> parsedElements = c.parseMeMoClassElements() })

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has been consumed and handled by the expression"
    parsedElements == fileElements
  }

  void "the consumer will not consume a MeMo class if the parent is not the one specified"() {
    given: "a cursor at a file in a technical document"
    MeMoClassElements fileElements = new MeMoClassElements()
    parser.parseElements() >> fileElements
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument, File), parser)

    and: "a visitor"
    MeMoClassElements parsedElements
    MeMoStreamVisitor visitor = MeMoStreamVisitorFactory.createConsumer(File.class, MainDocument.class,
            { c -> parsedElements = c.parseMeMoClassElements() })

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has not been consumed"
    parsedElements == null
  }
}
