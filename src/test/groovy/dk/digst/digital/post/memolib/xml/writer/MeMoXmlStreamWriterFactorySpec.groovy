package dk.digst.digital.post.memolib.xml.writer

import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.writer.FileContentLoader
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import spock.lang.Specification

class MeMoXmlStreamWriterFactorySpec extends Specification {

  void "a non validating writer with basic file content loader can be created"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when:
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(outputStream)

    then:
    writer != null
  }

  void "a validating writer with basic file content loader can be created"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when:
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(outputStream, true)

    then:
    writer != null
  }

  void "a non validating writer with external file content loader can be created"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()
    FileContentLoader loader = new FileContentLoader()

    when:
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(outputStream, loader)

    then:
    writer != null
  }

  void "a validating writer with external file content loader can be created"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()
    FileContentLoader loader = new FileContentLoader()

    when:
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(outputStream, loader, true, MeMoVersion.MEMO_VERSION_OLD)

    then:
    writer != null
  }
}
