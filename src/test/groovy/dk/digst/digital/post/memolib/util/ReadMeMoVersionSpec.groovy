package dk.digst.digital.post.memolib.util

import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import spock.lang.Specification

class ReadMeMoVersionSpec extends Specification {

  def "When reading the version of a MeMo, then a valid memoVersion is expected"() {
    when:
    MeMoVersion version = ReadMeMoVersion.getMeMoVersion(new BufferedInputStream(new FileInputStream(new File(pathname))))

    then:
    version == expectedResult

    where:
    pathname                                                                    | expectedResult
    "src/test/resources/xml/MeMo_Full_Example.xml"                              | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Full_Example_Invalid.xml"                      | MeMoVersion.UNKNOWN
    "src/test/resources/xml/MeMo_Full_Example_Invalid_memo_verion_char.xml"     | MeMoVersion.UNKNOWN
    "src/test/resources/xml/MeMo_Full_Example_V1.2.xml"                         | MeMoVersion.MEMO_VERSION_NEW
    "src/test/resources/xml/MeMo_Full_Example_V2.xml"                           | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie1_Eksempel.xml"                        | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie2_Eksempel_1af2.xml"                   | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie2_Eksempel_2af2.xml"                   | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie3_Eksempel.xml"                        | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_1af3.xml"                   | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_2af3.xml"                   | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_3af3.xml"                   | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie5_Eksempel.xml"                        | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Scenarie6_Eksempel.xml"                        | MeMoVersion.MEMO_VERSION_OLD
  }

  def "When reading the version of a MeMo, it is still possible to use the input stream"() {
    given:
    FileInputStream inputStream = new FileInputStream(new File(pathname))
    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream)
    when:
    MeMoVersion version = ReadMeMoVersion.getMeMoVersion(bufferedInputStream)

    and:
    MeMoVersion version2 = ReadMeMoVersion.getMeMoVersion(bufferedInputStream)
    Message result = MeMoParserFactory.createParser(bufferedInputStream).parse()

    then:
    version == expectedResult
    version2 == expectedResult

    and:
    noExceptionThrown()
    result

    where:
    pathname                                            | expectedResult
    "src/test/resources/xml/MeMo_Full_Example.xml"      | MeMoVersion.MEMO_VERSION_OLD
    "src/test/resources/xml/MeMo_Full_Example_V1.2.xml" | MeMoVersion.MEMO_VERSION_NEW

  }
}
