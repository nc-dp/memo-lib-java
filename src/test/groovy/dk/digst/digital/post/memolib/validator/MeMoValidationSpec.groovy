package dk.digst.digital.post.memolib.validator

import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.parser.MeMoParseException
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParserFactory
import spock.lang.Issue
import spock.lang.Specification

class MeMoValidationSpec extends Specification {

  private MeMoValidator validator

  private final String XML_VALIDATOR_DIR = "src/test/resources/xml/validation/"
  private final String XML_DIR = "src/test/resources/xml/"

  void setup() {}

  @Issue(["AC-6285", "AC-6288", "AC-6289"])
  void "Validate MeMo invalid header (subset of rules)"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + fileName)
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors and correct error codes have been collected"
    meMoValidatorErrors.size() == 1
    meMoValidatorErrors.any {
      it.errorCode == errorCode
      it.args == args
    }

    where:
    fileName                               | errorCode                                                         | args
    "MeMo_Invalid_Post_Type.xml"           | ValidationErrorCode.POST_TYPE_INVALID.errorCode()                 | new String[]{"POST"}
    "MeMo_Invalid_Nemsms_Notification.xml" | ValidationErrorCode.EMPTY_NOTIFICATION_NOT_ALLOWED.errorCode()    | new String[0]
    "MeMo_Invalid_Reply_Data.xml"          | ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND.errorCode() | new String[0]
    "MeMo_Invalid_Missing_Reply_Data.xml"  | ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND.errorCode() | new String[0]
  }

  void "Validate MeMo 1.1 validation doesn't validate invalid replyData"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Missing_Reply_Data_V1.1.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_OLD)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Errors have been collected"
    meMoValidatorErrors.isEmpty()
  }

  @Issue(["AC-6287"])
  void "Validate MeMo invalid ID types"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Id_Type.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors collected"
    meMoValidatorErrors.size() == 3

    and: "Correct error codes returned"
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.ID_TYPE_INVALID.errorCode()
      it.args == new String[]{"recipient", "CLR"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.ID_TYPE_INVALID.errorCode()
      it.args == new String[]{"sender", "CKR"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.ID_TYPE_INVALID.errorCode()
      it.args == new String[]{"sender.representative", "SSDASR"}
    }
  }

  @Issue(["AC-6290"])
  void "Validate MeMo invalid CVRs"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Cvr_Number.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors collected"
    meMoValidatorErrors.size() == 3

    and: "Correct error codes returned"
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.RECIPIENT_CPR_INVALID.errorCode()
      it.args == new String[]{"2211771212"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.REPRESENTATIVE_CPR_INVALID.errorCode()
      it.args == new String[]{"123-4678"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.SENDER_CPR_INVALID.errorCode()
      it.args == new String[]{"1234567"}
    }
  }

  @Issue(["AC-6291"])
  void "Validate MeMo invalid CPRs"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Cpr_Number.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors collected"
    meMoValidatorErrors.size() == 3

    and: "Correct error codes"
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.RECIPIENT_CPR_INVALID.errorCode()
      it.args == new String[]{"12345678901"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.REPRESENTATIVE_CPR_INVALID.errorCode()
      it.args == new String[]{"123-4678"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.SENDER_CPR_INVALID.errorCode()
      it.args == new String[]{"12345678"}
    }
  }

  @Issue(["AC-6283"])
  void "MeMo with invalid root fails hard"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Root_Element.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    when: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    parser.parse()

    then: "Exception thrown"
    thrown(MeMoParseException)
  }

  @Issue(["AC-6284"])
  void "MeMo with missing xmlns:memo fails hard"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Missing_Memo_Ns.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    when: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    parser.parse()

    then: "Exception thrown"
    thrown(MeMoParseException)
  }

  @Issue(["AC-6286"])
  void "Validate MeMo missing body"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Empty_Body.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors have been collected with correct error code"
    meMoValidatorErrors.size() == 1
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.MESSAGE_BODY_NOT_FOUND.errorCode()
      it.args == new String[0]
    }
  }

  void "MeMo with multiple errors returns a list of all errors"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Invalid_Multiple_Errors.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "Correct number of errors returned"
    meMoValidatorErrors.size() == 6

    and: "The correct error codes and error"
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.ID_TYPE_INVALID.errorCode()
              && it.args == new String[]{"sender.representative", "SSDASR"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.ID_TYPE_INVALID.errorCode()
              && it.args == new String[]{"sender.representative", "SSDASR"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.MESSAGE_BODY_NOT_FOUND.errorCode()
              && it.args == new String[0]
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.POST_TYPE_INVALID.errorCode()
              && it.args == new String[]{"POST"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.RECIPIENT_CVR_INVALID.errorCode()
              && it.args == new String[]{"2211771212"}
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND.errorCode()
              && it.args == new String[0]
    }
    meMoValidatorErrors.any {
      it.errorCode == ValidationErrorCode.SENDER_CPR_INVALID.errorCode()
              && it.args == new String[]{"12345678"}
    }
  }

  void "Validate a valid MeMo v1.2"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example_V1.2.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "No errors are returned"
    meMoValidatorErrors.isEmpty()
  }

  void "Validate a valid MeMo v1.1"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_OLD)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "No errors are returned"
    meMoValidatorErrors.isEmpty()
  }

  void "MeMoValidator constructor with just MeMoVersion works"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_OLD)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(MeMoVersion.MEMO_VERSION_OLD)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate(message)

    then: "No errors are returned"
    meMoValidatorErrors.isEmpty()
  }

  void "MeMoValidator constructor with just MeMoVersion throws exception if message is not provided"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_OLD)
    parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(MeMoVersion.MEMO_VERSION_OLD)

    when: "Validate message"
    validator.validate()

    then: "No errors are returned"
    thrown(MeMoValidationException)
  }

  void "Validate a valid NEMSMS"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_VALIDATOR_DIR + "MeMo_Valid_Nemsms.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "fileInputStream is parsed into a memo-lib Message"
    MeMoParser parser = MeMoXmlStreamParserFactory.createParser(fileInputStream, false, MeMoVersion.MEMO_VERSION_NEW)
    Message message = parser.parse()

    and: "A validator is declared and initialized"
    validator = MeMoValidatorFactory.createMeMoValidator(message)

    when: "Validate message"
    def meMoValidatorErrors = validator.validate()

    then: "No errors are returned"
    meMoValidatorErrors.isEmpty()
  }
}