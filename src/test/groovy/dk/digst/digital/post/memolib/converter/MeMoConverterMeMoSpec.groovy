package dk.digst.digital.post.memolib.converter

import dk.digst.digital.post.memolib.builder.FileBuilder
import dk.digst.digital.post.memolib.builder.FileContentBuilder
import dk.digst.digital.post.memolib.builder.MainDocumentBuilder
import dk.digst.digital.post.memolib.builder.MessageBodyBuilder
import dk.digst.digital.post.memolib.builder.MessageBuilder
import dk.digst.digital.post.memolib.builder.MessageHeaderBuilder
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.util.MeMoStreamUtils
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import dk.digst.digital.post.memolib.xml.writer.MeMoXmlWriterFactory
import spock.lang.Shared
import spock.lang.Specification

class MeMoConverterMeMoSpec extends Specification {

  public static final int ONE_MEGA_BIT = 1048576
  private FileInputStream inputStreamOld
  private FileInputStream inputStreamNew
  @Shared
  private String path = "build/tmp/LargeFileForConversion.xml"

  void setupSpec() {
    java.io.File file = new java.io.File(path)
    Message message = build100MbMeMo()
    OutputStream stream = new FileOutputStream(file)
    MeMoStreamWriter writer = MeMoXmlWriterFactory.createWriter(stream)
    writer.write(message)
    writer.close()
    stream.close()
  }

  void setup() {
    java.io.File memoOld = new java.io.File("src/test/resources/xml/MeMo_Full_Example.xml")
    java.io.File memoNew = new java.io.File("src/test/resources/xml/MeMo_Full_Example_V1.2.xml")
    inputStreamOld = new FileInputStream(memoOld)
    inputStreamNew = new FileInputStream(memoNew)
  }

  void "A Up converted message can be created into input stream, at no additional memory cost"() {
    given: "A message from external source"
    long startMemoryUsage = getCurrentMemoryUsageInMb()
    MeMoConverter converter = MeMoConvertionFactory.createUpConverter(inputStreamOld)

    when: "converted"
    Message message = converter.parseIntoConvertedObject()
    long messageUsage = getCurrentMemoryUsageInMb() - startMemoryUsage

    and: "converted is converted to stream"
    InputStream memoInput = MeMoStreamUtils.getMessageAsInputStream(message)
    long inputStreamUsage = getCurrentMemoryUsageInMb() - startMemoryUsage

    then:
    (inputStreamUsage - messageUsage) <= 1
  }

  void "A Down converted message can be created into input stream, at no additional memory cost"() {
    given: "A message from external source"
    long startMemoryUsage = getCurrentMemoryUsageInMb()
    MeMoConverter converter = MeMoConvertionFactory.createDownConverter(inputStreamNew)

    when: "converted"
    Message message = converter.parseIntoConvertedObject()
    long messageUsage = getCurrentMemoryUsageInMb() - startMemoryUsage

    and: "converted is converted to stream"
    InputStream memoInput = MeMoStreamUtils.getMessageAsInputStream(message)
    long inputStreamUsage = getCurrentMemoryUsageInMb() - startMemoryUsage

    then:
    (inputStreamUsage - messageUsage) <= 1
  }

  void "Converting messages with large files, input stream will be copied over directly"() {
    given: "A message from external source"
    java.io.File file = new java.io.File(path)
    FileInputStream stream = new FileInputStream(file)

    and:
    long startMemoryUsage = getCurrentMemoryUsageInMb()
    MeMoConverter converter = MeMoConvertionFactory.createUpConverter(stream)

    when: "converted into stream"
    converter.parseIntoConvertedStream()
    long messageUsage = getCurrentMemoryUsageInMb() - startMemoryUsage

    then: "Memory is at most one full memo"
    messageUsage <= 200
  }

  private static long getCurrentMemoryUsageInMb() {
    Runtime runtime = Runtime.getRuntime()
    runtime.gc()
    return (runtime.totalMemory() - runtime.freeMemory()) / (ONE_MEGA_BIT)
  }

  private static Message build100MbMeMo() {
    MessageBuilder builder = MessageBuilder.newBuilder().messageHeader(MessageHeaderBuilder.newBuilder().build())

    List<File> files = new ArrayList<>()
    for (int i = 0; i < 100; i++) {
      files.add(FileBuilder.newBuilder().content(
              FileContentBuilder.newBuilder().location("src/test/resources/files/1mbfile.xml").build()).build())
    }
    builder
            .memoVersion(1.1)
            .memoSchVersion(MeMoVersion.MEMO_SCH_VERSION)
    return builder.messageBody(MessageBodyBuilder.newBuilder().mainDocument(
            MainDocumentBuilder.newBuilder().file(files).build()).build()).build()
  }
}
