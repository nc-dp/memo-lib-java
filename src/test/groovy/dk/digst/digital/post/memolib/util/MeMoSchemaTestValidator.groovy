package dk.digst.digital.post.memolib.util

import javax.xml.transform.stream.StreamSource
import javax.xml.validation.Validator

class MeMoSchemaTestValidator {

  private final Validator validator

  MeMoSchemaTestValidator(Validator validator) {
    this.validator = validator
  }

  boolean isValid(InputStream inputStream) {
    try {
      validator.validate(new StreamSource(inputStream))
      return true
    } catch (Exception e) {
      return false
    }
  }
}
