package dk.digst.digital.post.memolib.xml.writer

import dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.util.ResultParser
import java.time.LocalDateTime
import spock.lang.Shared
import spock.lang.Specification

class MessageBodyWriterSpec extends Specification {

  @Shared
  MeMoStreamWriterImpl meMoStreamWriter

  @Shared
  ByteArrayOutputStream output

  void setup() {
    output = new ByteArrayOutputStream()
    meMoStreamWriter = MeMoXmlWriterFactory.createWriter(output) as MeMoStreamWriterImpl
  }

  void cleanup() {
    meMoStreamWriter.closeStream()
  }

  def 'write message body with a single digital document'() {
    given:
    MeMoStreamWriterImpl writer = meMoStreamWriter.writeStartOfMessage(new Message(memoSchVersion: MeMoVersion.MEMO_SCH_VERSION,
                                                                                   memoVersion: MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber()))
    LocalDateTime now = LocalDateTime.now()
    MessageBody messageBody = PrefilledMessageBuilderFactory
            .messageBody()
            .createdDateTime(now)
            .build()

    when: 'write the digital document to the MeMo message'
    writer.writeBody(messageBody)

    and: 'close and flush the writer'
    meMoStreamWriter.closeStream()

    then:
    Message message = ResultParser.parseOutputMessage(output)
    with(message.messageBody) {
      messageBody.createdDateTime == now
      messageBody.mainDocument != null
    }
  }
}
