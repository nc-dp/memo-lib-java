package dk.digst.digital.post.memolib.xml.visitor

import dk.digst.digital.post.memolib.model.Action
import dk.digst.digital.post.memolib.model.AdditionalReplyData
import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.Reservation
import dk.digst.digital.post.memolib.model.TechnicalDocument
import spock.lang.Specification

class MeMoStreamVisitorFactorySpec extends Specification {
  void "The factory can create a consumer for a MeMo class"() {
    when:
    MeMoStreamVisitor consumer = MeMoStreamVisitorFactory.createConsumer(Action.class, { cursor -> })

    then:
    consumer != null
  }

  void "The factory can create a consumer for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitor consumer = MeMoStreamVisitorFactory.createConsumer(TechnicalDocument.class, Action.class, { cursor -> })

    then:
    consumer != null
  }

  void "The factory can create a processor for a MeMo class"() {
    when:
    MeMoStreamProcessorVisitor processor = MeMoStreamVisitorFactory.createProcessor(TechnicalDocument.class)

    then:
    processor != null
    processor.getResult().isEmpty()
  }

  void "The factory can create a processor for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamProcessorVisitor processor = MeMoStreamVisitorFactory.createProcessor(TechnicalDocument.class, Action.class)

    then:
    processor != null
  }

  void "clazz must be passed to The factory method when creating the consumer"() {
    when:
    MeMoStreamVisitorFactory.createConsumer(null, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "clazz is marked non-null but is null"
  }

  void "consumer must be passed to The factory method when creating the consumer"() {
    when:
    MeMoStreamVisitorFactory.createConsumer(AdditionalReplyData.class, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "consumer is marked non-null but is null"
  }

  void "clazz must be passed to The factory method when creating the consumer for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createConsumer(null, null, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "clazz is marked non-null but is null"
  }

  void "parentClazz must be passed to The factory method when creating the consumer for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createConsumer(Action.class, null, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "parentClazz is marked non-null but is null"
  }

  void "consumer must be passed to The factory method when creating the consumer for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createConsumer(Action.class, Reservation.class, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "consumer is marked non-null but is null"
  }

  void "clazz must be passed to The factory method when creating the processor for a MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createProcessor(null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "clazz is marked non-null but is null"
  }

  void "clazz must be passed to The factory method when creating the processor for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createProcessor(null, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "clazz is marked non-null but is null"
  }

  void "parentClazz must be passed to The factory method when creating the processor for a MeMo class which has a certain parent MeMo class"() {
    when:
    MeMoStreamVisitorFactory.createProcessor(MainDocument.class, null)

    then:
    NullPointerException exception = thrown(NullPointerException)
    exception.message == "parentClazz is marked non-null but is null"
  }
}
