package dk.digst.digital.post.memolib.writer

import dk.digst.digital.post.memolib.util.MeMoTestDataProvider
import spock.lang.Specification

class DefaultFileContentResolverSpec extends Specification {

  void "The valid file path of a file can be resolved correctly"() {
    given:
    String filePath = MeMoTestDataProvider.modelPdf();
    DefaultFileContentResolver resolver = new DefaultFileContentResolver()

    when:
    InputStream stream = resolver.resolve(filePath)

    then: "the file has been resolved and can be read"
    stream != null
    stream.available() > 0

    cleanup:
    stream.close()
  }

  void "Another location which can not be resolved to a file path results in null being returned"() {
    given:
    DefaultFileContentResolver resolver = new DefaultFileContentResolver()

    when:
    InputStream stream = resolver.resolve("http://host.domain")

    then: "the file content has not been resolved"
    stream == null
  }
}
