package dk.digst.digital.post.memolib.xml.parser

import dk.digst.digital.post.memolib.model.AdditionalDocument
import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.model.MessageHeader
import dk.digst.digital.post.memolib.model.ReplyData
import dk.digst.digital.post.memolib.model.Sender
import dk.digst.digital.post.memolib.model.TechnicalDocument
import spock.lang.Specification

class MeMoXmlStreamLocationSpec extends Specification {

  void "An empty location can be created"() {
    when: "a location is created"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation()

    then: "information about the location can be retrieved"
    !location.parentPosition.isPresent()
    location.depth == 0
  }

  void "Trying to get current position for an empty location result in an exception being thrown"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation()

    when: "the current position is read"
    location.currentPosition

    then: "an exception is thrown"
    IllegalStateException exception = thrown(IllegalStateException)
    exception.message == "The element stack is empty"

  }

  void "the MeMo Class and depth in the tree-structure can be retrieved"() {
    when: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File)

    then: "information about the location can be retrieved"
    location.currentPosition == File
    location.parentPosition.orElseThrow() == MainDocument
    location.depth == 4
  }

  void "a child element can be added to the location"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody)

    when: "the stack is updated"
    location.updateStack(3, MainDocument)

    then: "the current location has changed"
    location.currentPosition == MainDocument
    location.parentPosition.orElseThrow() == MessageBody
    location.depth == 3
  }

  void "a sibling can be added to the location"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument)

    when: "the stack is updated"
    location.updateStack(3, TechnicalDocument)

    then: "the current location has changed"
    location.currentPosition == TechnicalDocument
    location.parentPosition.orElseThrow() == MessageBody
    location.depth == 3
  }

  void "a MeMo class with lesser depth can be added to the location"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageHeader, Sender)

    when: "the stack is updated"
    location.updateStack(2, MessageBody)

    then: "the current location has changed"
    location.currentPosition == MessageBody
    location.parentPosition.orElseThrow() == Message
    location.depth == 2
  }

  void "depth can not be set to 0"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageHeader, Sender)

    when: "the stack is updated"
    location.updateStack(0, ReplyData)

    then: "an exception is thrown"
    IllegalArgumentException exception = thrown(IllegalArgumentException)
    exception.message == "depth must 1 or greater"
  }

  void "location pointing to root element does not have a parent"() {
    when: "a location is created"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message)

    then: "parent is empty"
    !location.parentPosition.isPresent()
  }

  void "the position can be updated multiple times"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message)

    when: "the stack is updated"
    location.updateStack(2, MessageHeader)

    then: "the current location has changed"
    location.currentPosition == MessageHeader
    location.parentPosition.orElseThrow() == Message
    location.depth == 2

    when: "the stack is updated"
    location.updateStack(3, Sender)

    then: "the current location has changed"
    location.currentPosition == Sender
    location.parentPosition.orElseThrow() == MessageHeader
    location.depth == 3

    when: "the stack is updated"
    location.updateStack(2, MessageBody)

    then: "the current location has changed"
    location.currentPosition == MessageBody
    location.parentPosition.orElseThrow() == Message
    location.depth == 2

    when: "the stack is updated"
    location.updateStack(3, MainDocument)

    then: "the current location has changed"
    location.currentPosition == MainDocument
    location.parentPosition.orElseThrow() == MessageBody
    location.depth == 3

    when: "the stack is updated"
    location.updateStack(3, AdditionalDocument)

    then: "the current location has changed"
    location.currentPosition == AdditionalDocument
    location.parentPosition.orElseThrow() == MessageBody
    location.depth == 3

    when: "the stack is updated"
    location.updateStack(3, TechnicalDocument)

    then: "the current location has changed"
    location.currentPosition == TechnicalDocument
    location.parentPosition.orElseThrow() == MessageBody
    location.depth == 3
  }

  void "the position can be copied"() {
    given: "a location"
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument)

    when: "the stack is copied"
    MeMoXmlStreamLocation newLocation = new MeMoXmlStreamLocation(location)

    then:
    location.currentPosition == newLocation.currentPosition
    location.parentPosition.orElseThrow() == newLocation.parentPosition.orElseThrow()
    location.depth == newLocation.depth

    when: "the new location is updated"
    newLocation.updateStack(4, File)

    then: "the initial position is not modified"
    location.currentPosition != newLocation.currentPosition
    location.currentPosition == MainDocument
    location.parentPosition.orElseThrow() != newLocation.parentPosition.orElseThrow()
    location.parentPosition.orElseThrow() == MessageBody
    location.depth != newLocation.depth
    location.depth == 3
  }
}
