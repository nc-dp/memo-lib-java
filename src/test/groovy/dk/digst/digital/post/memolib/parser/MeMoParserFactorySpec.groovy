package dk.digst.digital.post.memolib.parser


import spock.lang.Specification

class MeMoParserFactorySpec extends Specification {

  void "the factory can create a parser for an input stream"() {
    expect:
    MeMoParserFactory.createParser(new ByteArrayInputStream(), false) != null
  }

  void "the factory can create a parser with validation enabled"() {
    expect:
    MeMoParserFactory.createParser(new ByteArrayInputStream(),true) != null
  }
}
