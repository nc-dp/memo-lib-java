package dk.digst.digital.post.memolib.xml.writer

import dk.digst.digital.post.memolib.builder.MessageBodyBuilder
import dk.digst.digital.post.memolib.builder.MessageBuilder
import dk.digst.digital.post.memolib.config.GlobalConfig
import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.FileContent
import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.MeMoVersion
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.model.Representative
import dk.digst.digital.post.memolib.parser.MeMoParser
import dk.digst.digital.post.memolib.parser.MeMoParserFactory
import dk.digst.digital.post.memolib.util.MeMoSchemaTestProvider
import dk.digst.digital.post.memolib.util.MeMoSchemaTestValidator
import dk.digst.digital.post.memolib.util.MeMoTestDataProvider
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter
import dk.digst.digital.post.memolib.writer.MeMoWriteException
import dk.digst.digital.post.memolib.writer.MeMoWriterFactory
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParserFactory
import java.time.LocalDateTime
import org.xmlunit.builder.DiffBuilder
import org.xmlunit.diff.Diff
import org.xmlunit.util.Linqy
import spock.lang.Issue
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.action
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.entryPoint
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.file
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.fileContent
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.mainDocument
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.message
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.messageBody
import static dk.digst.digital.post.memolib.builder.PrefilledMessageBuilderFactory.messageHeader

class MeMoXmlStreamWriterSpec extends Specification {
  @Shared
  MeMoSchemaTestValidator xmlSchemaValidator
  @Shared
  ByteArrayOutputStream output
  @Shared
  MeMoStreamWriter xmlWriter

  void setup() {
    xmlSchemaValidator = MeMoSchemaTestProvider.getXmlSchemaValidator()
    output = new ByteArrayOutputStream()
    xmlWriter = MeMoXmlWriterFactory.createWriter(output)
  }

  void cleanup() {
    xmlWriter.closeStream()
  }

  @Issue("AC-230")
  void 'Valid MeMo message can be generated'() {
    given:
    Message customMessage = message().build()

    when:
    xmlWriter.write(customMessage)

    and:
    xmlWriter.closeStream()

    then:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))
  }

  @Issue("AC-893")
  void 'Valid MeMo message can be generated with no body'() {
    given: 'a message without a body'
    Message customMessage = message().messageBody(null).build()

    when: 'the message is written to an outputstream'
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    then: 'the outputstream contains a valid memo message'
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))
  }


  void 'If MeMo is valid and validation is enabled, valid memo is generated'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoXmlWriterFactory.createWriter(validatingWriterOutput, true)
    Message customMessage = message().build()

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    noExceptionThrown()

    and:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(validatingWriterOutput.toByteArray()))
  }

  void 'If MeMo is not valid and validation is enabled, writer throws an exception'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoXmlWriterFactory.createWriter(validatingWriterOutput, true)

    and:
    Message customMessage = message().build()
    customMessage.messageHeader.messageUUID = null

    and:
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    expect:
    !xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    then:
    MeMoWriteException expectedException = thrown(MeMoWriteException)
    expectedException.message.contains("tag name \"messageID\" is not allowed. Possible tag names are: <messageUUID>")
  }

  void 'An external file reference can be included and a valid MeMo message can be generated'() {
    given:
    FileContent fileContent = fileContent()
            .location(MeMoTestDataProvider.modelPdf())
            .build()
    File file = file()
            .content(fileContent)
            .build()
    MainDocument mainDocument = mainDocument()
            .addFile(file)
            .build()
    MessageBody messageBody = messageBody()
            .mainDocument(mainDocument)
            .build()
    Message customMessage = message()
            .messageBody(messageBody)
            .build()

    when:
    xmlWriter.write(customMessage)

    and:
    xmlWriter.closeStream()

    then:
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))
  }

  void 'When writing a new MeMo to latest version with validation, then it is valid, has correct version number and does not have a schematron version defined'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoXmlWriterFactory.createNewWriter(validatingWriterOutput, true)

    and:
    Message customMessage = message().build()
    customMessage.messageHeader.sender.representative = new Representative(representativeId: "1234567890", idType: "CVR", idTypeLabel: "IDe", label: "label")

    and: "consume the message, (will crash otherwise)"
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    and:
    Message result = MeMoParserFactory.createParser(new ByteArrayInputStream(validatingWriterOutput.toByteArray())).parse()

    then:
    noExceptionThrown()
    result.memoVersion == BigDecimal.valueOf(1.2)
    result.memoSchVersion == null
    result.messageHeader.sender.representative
  }

  void 'When writing a new MeMo to latest version, then has correct version number and does not have a schematron version defined'() {
    given:
    ByteArrayOutputStream validatingWriterOutput = new ByteArrayOutputStream()
    MeMoStreamWriter validatingXmlWriter = MeMoXmlWriterFactory.createNewWriter(validatingWriterOutput, false)

    and:
    Message customMessage = message().build()
    customMessage.messageHeader.sender.representative = new Representative(representativeId: "1234567890", idType: "CVR", idTypeLabel: "IDe", label: "label")

    and: "consume the message, (will crash otherwise)"
    xmlWriter.write(customMessage)
    xmlWriter.closeStream()

    when:
    validatingXmlWriter.write(customMessage)

    and:
    validatingXmlWriter.closeStream()

    and:
    Message result = MeMoParserFactory.createParser(new ByteArrayInputStream(validatingWriterOutput.toByteArray())).parse()

    then:
    noExceptionThrown()
    result.memoVersion == BigDecimal.valueOf(1.2)
    result.memoSchVersion == null
    result.messageHeader.sender.representative

  }

  @Issue(["AC-578", "AC-579"])
  @Unroll
  void 'MeMo XML examples can be de-serialized and then serialized without losing any content or its validity (#exampleName)'() {
    when: 'XML is de-serialized to java classes'
    java.io.File originalXML = new java.io.File(pathname)
    FileInputStream fileInputStream = new FileInputStream(originalXML)
    MeMoParser xmlParser = MeMoXmlStreamParserFactory.createParser(fileInputStream)
    Message message = xmlParser.parse()

    and: 'the message is then serialized back to XML'
    xmlWriter.write(message)
    xmlWriter.closeStream()
    String serializedMessage = output.toString("UTF-8")

    then: 'It is still a valid XML'
    xmlSchemaValidator.isValid(new ByteArrayInputStream(output.toByteArray()))

    and: 'It is identical to the original XML'
    Diff myDiff = DiffBuilder
            .compare(originalXML)
            .withTest(serializedMessage)
            .normalizeWhitespace()
            .ignoreComments()
            .build()
    Linqy.count(myDiff.getDifferences()) == 0

    where:
    pathname                                                  | exampleName
    "src/test/resources/xml/MeMo_Full_Example.xml"            | '"Full example"'
    "src/test/resources/xml/MeMo_Scenarie1_Eksempel.xml"      | '"Scenario 1 - Example 1 of 1"'
    "src/test/resources/xml/MeMo_Scenarie2_Eksempel_1af2.xml" | '"Scenario 2 - Example 1 of 2"'
    "src/test/resources/xml/MeMo_Scenarie2_Eksempel_2af2.xml" | '"Scenario 2 - Example 2 of 2"'
    "src/test/resources/xml/MeMo_Scenarie3_Eksempel.xml"      | '"Scenario 3 - Example 1 of 1"'
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_1af3.xml" | '"Scenario 4 - Example 1 of 3"'
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_2af3.xml" | '"Scenario 4 - Example 2 of 3"'
    "src/test/resources/xml/MeMo_Scenarie4_Eksempel_3af3.xml" | '"Scenario 4 - Example 3 of 3"'
    "src/test/resources/xml/MeMo_Scenarie5_Eksempel.xml"      | '"Scenario 5 - Example 1 of 1"'
    "src/test/resources/xml/MeMo_Scenarie6_Eksempel.xml"      | '"Scenario 6 - Example 1 of 1"'
  }

  void 'the writer does not close the file input stream'() {
    given:
    Message message = message().build()
    xmlWriter.write(message)

    when: 'when closing the writer and then adding to the stream'
    xmlWriter.close()
    output.write(1)

    then: 'No exception is thrown and the writer is indeed closed'
    noExceptionThrown()
    xmlWriter.isClosed()

    cleanup:
    xmlWriter.closeStream()
  }

  void 'calling close twice does not cause an exception'() {
    given:
    Message message = message().build()
    xmlWriter.write(message)

    when: 'Closing the writer twice'
    xmlWriter.close()
    xmlWriter.close()

    then: 'No exception is thrown'
    noExceptionThrown()
    xmlWriter.isClosed()
  }
}
