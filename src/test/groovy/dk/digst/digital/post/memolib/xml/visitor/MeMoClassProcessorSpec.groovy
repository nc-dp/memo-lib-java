package dk.digst.digital.post.memolib.xml.visitor

import dk.digst.digital.post.memolib.model.File
import dk.digst.digital.post.memolib.model.MainDocument
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.model.TechnicalDocument
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamCursor
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamLocation
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParser
import dk.digst.digital.post.memolib.xml.stax.Stax2FactoryProvider
import spock.lang.Shared
import spock.lang.Specification

class MeMoClassProcessorSpec extends Specification {

  @Shared MeMoXmlStreamParser parser

  void setup() {
    parser = Mock(MeMoXmlStreamParser,
            constructorArgs: [null, new ByteArrayInputStream(), Stax2FactoryProvider.createXMLStreamReaderFactory(false)])
  }

  void cleanup() {
    parser.close()
  }

  void "the processor can visit a MeMo class"() {
    given: "a cursor at a file in main document"
    File file = new File()
    parser.parseClass(_) >> file
    MeMoXmlStreamLocation location = new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File)
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", location, parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<File> visitor = MeMoStreamVisitorFactory.createProcessor(File.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has been processed"
    visitor.getResult() == [file]
  }

  void "the processor will not consume another MeMo class than the one specified"() {
    given: "a cursor at a technical document"
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("TechnicalDocument", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<TechnicalDocument> visitor = MeMoStreamVisitorFactory.createProcessor(File.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has not been consumed"
    visitor.getResult() == []
  }

  void "the processor can visit a MeMo class and process it if the parent MeMo class is the same as the one specified"() {
    given: "a cursor at a file in main document"
    File file = new File()
    parser.parseClass(_) >> file
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<File> visitor = MeMoStreamVisitorFactory.createProcessor(File.class, MainDocument.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has been consumed and handled by the expression"
    visitor.getResult() == [file]
  }

  void "the processor will not process a MeMo class if the parent is not the one specified"() {
    given: "a cursor at a file in a technical document"
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument, File), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<File> visitor = MeMoStreamVisitorFactory.createProcessor(File.class, MainDocument.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the cursor has not been consumed"
    visitor.getResult() == []
  }

  void "the processor can return a single result"() {
    given: "a cursor at main document"
    MainDocument mainDocument = new MainDocument()
    parser.parseClass(_) >> mainDocument
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("MainDocument", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<MainDocument> visitor = MeMoStreamVisitorFactory.createProcessor(MainDocument.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the result of the processing is a single object"
    visitor.getSingleResult().isPresent()
    visitor.getSingleResult().orElseThrow() == mainDocument
  }

  void "A single result can be empty"() {
    given: "a cursor at main document"
    MeMoXmlStreamCursor cursor = new MeMoXmlStreamCursor("TechnicalDocument", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<MainDocument> visitor = MeMoStreamVisitorFactory.createProcessor(MainDocument.class)

    when: "the cursor accepts the visitor"
    cursor.accept(visitor)

    then: "the result of the processing is empty"
    !visitor.getSingleResult().isPresent()
  }

  void "An exception is thrown if the result has more than one object and we try to get single result"() {
    given: "two cursors at files"
    File file = new File()
    parser.parseClass(_) >> file
    MeMoXmlStreamCursor cursor1 = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, MainDocument, File), parser)
    MeMoXmlStreamCursor cursor2 = new MeMoXmlStreamCursor("File", new MeMoXmlStreamLocation(Message, MessageBody, TechnicalDocument, File), parser)

    and: "a visitor"
    MeMoStreamProcessorVisitor<File> visitor = MeMoStreamVisitorFactory.createProcessor(File.class)

    when: "the cursors accept the visitor"
    cursor1.accept(visitor)
    cursor2.accept(visitor)

    and: "we try to get a single result"
    visitor.getSingleResult()

    then: "an exception is thrown"
    IllegalStateException exception = thrown(IllegalStateException)
    exception.message == "size of collected objects is 2"
  }
}
