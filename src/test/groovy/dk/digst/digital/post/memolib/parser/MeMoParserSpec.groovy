package dk.digst.digital.post.memolib.parser

import com.ctc.wstx.msv.W3CSchemaFactory
import dk.digst.digital.post.memolib.config.GlobalConfig
import dk.digst.digital.post.memolib.model.Address
import dk.digst.digital.post.memolib.model.FileContent
import dk.digst.digital.post.memolib.model.Message
import dk.digst.digital.post.memolib.model.MessageBody
import dk.digst.digital.post.memolib.model.MessageHeader
import dk.digst.digital.post.memolib.model.MessageType
import dk.digst.digital.post.memolib.model.Reservation
import dk.digst.digital.post.memolib.xml.schema.MeMoValidationSchemaProvider
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitor
import dk.digst.digital.post.memolib.xml.visitor.MeMoStreamVisitorFactory
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.OffsetDateTime
import org.codehaus.stax2.validation.XMLValidationSchema
import org.codehaus.stax2.validation.XMLValidationSchemaFactory
import spock.lang.Issue
import spock.lang.Shared
import spock.lang.Specification

import static org.codehaus.stax2.validation.XMLValidationSchema.SCHEMA_ID_W3C_SCHEMA

class MeMoParserSpec extends Specification {

  public static final String XML_DIR = "src/test/resources/xml/"

  @Shared private Message parsedMessage

  void setup() {
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)
    MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, false)
    parsedMessage = parser.parse()
  }

  @Issue("AC-231")
  void "The MessageHeader attributes is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "Parsing the attributes in MessageHeader"
    with(messageHeader) {
      messageType == MessageType.DIGITALPOST
      messageUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194c114")
      messageId == "MSG-12345"
      messageCode == "Pladsanvisning"
      label == "Besked fra Børneforvaltningen"
      notification == "Du har fået digitalpost fra Kommunen vedr. din ansøgning om børnehaveplads."
      reply
      replyByDateTime == asLocalDateTime("2018-09-30T12:00:00Z")
      doNotDeliverUntilDate == LocalDate.parse("2018-09-15")
    }
  }

  @Issue("AC-231")
  void "The MessageHeader ContentData is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "Parsing the contentData"
    with(messageHeader.contentData) {
      cprData.cprNumber == "2512169996"
      cprData.name == "Emilie Hansen"
      cvrData.cvrNumber == "12345678"
      cvrData.companyName == "[Virksomhed]"
      motorVehicle.licenseNumber == "AB12345"
      motorVehicle.chassisNumber == "WFR18ZZ67W094959"
      propertyNumber.propertyNumber == "ABC1234"
      caseId.caseId == "SAG-12345"
      caseId.caseSystem == "Sagssystem 1234"
      kleData.version == "Maj 2018"
      kleData.subjectKey == "00.00.00"
      kleData.activityFacet == "[Tekst]"
      kleData.label == "[KLE tekst]"
      formData.taskKey == "00.00.00.00"
      formData.version == "Opgavenøglen v2.12"
      formData.activityFacet == "[Tekst]"
      formData.label == "[FORM tekst]"
      productionUnit.productionUnitNumber == 1234567890
      productionUnit.productionUnitName == "[Produktionsenhed]"
      education.educationCode == "123ABC"
      education.educationName == "[Uddannelse navn]"

      with(address) {
        id == "da1c15bb-f74d-4a26-8617-4fbb5ac4f063"
        addressLabel == "Søen"
        houseNumber == "45"
        door == "tv"
        floor == "0"
        co == "AB1"
        zipCode == "5000"
        city == "Odense"
        country == "DK"
      }

      with(additionalContentData[0]) {
        contentDataType == "Liste A"
        contentDataName == "Navn 1"
        contentDataValue == "Værdi 1"
      }

      with(additionalContentData[1]) {
        contentDataType == "Liste A"
        contentDataName == "Navn 2"
        contentDataValue == "Værdi 2"
      }
    }
  }

  @Issue(["AC-231", "AC-2515"])
  void "The MessageHeader ForwardData is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "Parsing the forwardData"
    with(messageHeader.forwardData) {
      messageUUID == UUID.fromString('8c2ea15d-61fb-4ba9-9366-42f8b194c114')
      originalSender == 'Kommunen'
      originalContentResponsible == 'Børnehaven, Tusindfryd'
      contactPointId == "1234.1234"
      comment == 'kommentar til modtageren'
    }
  }

  @Issue(["AC-231", "AC-2514", "AC-2516", "AC-2517"])
  void "The MessageHeader Sender is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "The Sender and child elements are parsed correctly"
    with(messageHeader.sender) {
      senderId == "12345678"
      idType == "CVR"
      idTypeLabel == "eID"
      label == "Kommunen"

      with(attentionData) {
        attentionPerson.personId == "9000001234"
        attentionPerson.label == "Hans Hansen"

        productionUnit.productionUnitNumber == 1234567890
        productionUnit.productionUnitName == "Produktionsenhed A"

        globalLocationNumber.globalLocationNumber == 5798000012345L
        globalLocationNumber.location == "Kommune A"

        email.emailAddress == "info@tusindfryd.dk"
        email.relatedAgent == "Hans Hansen"

        seNumber.seNumber == "12345678"
        seNumber.companyName == "Kommune A"

        telephone.telephoneNumber == "12345678"
        telephone.relatedAgent == "Hans Hansen"

        eidData.eid == "CVR:12345678-RID:1234567890123"
        eidData.label == "Kommune A"

        contentResponsible.contentResponsibleId == "22334455"
        contentResponsible.label == "Børnehaven, Tusindfryd"

        generatingSystem.generatingSystemId == "Sys-1234"
        generatingSystem.label == "KommunaltPostSystem"

        sorData.sorIdentifier == "468031000016004"
        sorData.entryName == "tekst"

        with(address) {
          id == "8c2ea15d-61fb-4ba9-9366-42f8b194c852"
          addressLabel == "Gaden"
          houseNumber == "7A"
          door == "th"
          floor == "3"
          co == "C/O"
          zipCode == "9000"
          city == "Aalborg"
          country == "DK"

          with(addressPoint) {
            geographicEastingMeasure == "557501.23"
            geographicNorthingMeasure == "6336248.89"
            geographicHeightMeasure == "0.0"
          }
        }
      }
    }
  }

  @Issue("AC-231")
  void "The MessageHeader Recipient is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "The Recipient and child elements are parsed correctly"
    with(messageHeader.recipient) {
      recipientId == "2211771212"
      idType == "CPR"
      idTypeLabel == "eID"
      label == "Mette Hansen"

      with(attentionData) {
        attentionPerson.personId == "2211771212"
        attentionPerson.label == "Mette Hansen"

        productionUnit.productionUnitNumber == 1234567890
        productionUnit.productionUnitName == "[Produktionsenhed]"

        globalLocationNumber.globalLocationNumber == 5798000012345L
        globalLocationNumber.location == "[Navn på lokation]"

        email.emailAddress == "m.hansen@gmail.com"
        email.relatedAgent == "Mette Hansen"

        seNumber.seNumber == "12345678"
        seNumber.companyName == "[Virksomhed]"

        telephone.telephoneNumber == "12345678"
        telephone.relatedAgent == "Mette Hansen"

        eidData.eid == "12345678_1234567890"
        eidData.label == "[Virksomhed]"

        contentResponsible.contentResponsibleId == "22334455"
        contentResponsible.label == "[Ansvarlig]"

        sorData.sorIdentifier == "468031000016004"
        sorData.entryName == "[SOR tekst]"

        unstructuredAddress.unstructured == "Bakketoppen 6, 9000 Aalborg"

        with(contactPoint) {
          contactGroup == "22.33.44.55"
          contactPointId == "241d39f6-998e-4929-b198-ccacbbf4b330"
          label == "Kommunen, Pladsanvisningen"

          contactInfo[0].label == "Barnets CPR nummer"
          contactInfo[0].value == "2512169996"
          contactInfo[1].label == "Barnets navn"
          contactInfo[1].value == "Emilie Hansen"
        }
      }
    }
  }

  @Issue(["AC-231", "AC-2512", "AC-2513"])
  void "The MessageHeader ReplyData is parsed correctly"() {
    when:
    MessageHeader messageHeader = parsedMessage.messageHeader

    then: "The Recipient and child elements are parsed correctly"
    with(messageHeader.replyData[0]) {
      messageId == "MSG-12344"
      messageUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194c114")
      senderId == "12345678"
      recipientId == "1234567890"
      caseId == "SAG-456"
      contactPointId == "1234567890"
      generatingSystemId == "ABC-123"
      comment == "Tilbud om børnehaveplads til Emilie Hansen"
      additionalReplyData.size() == 2
      additionalReplyData[0].label == "Intern note"
      additionalReplyData[0].value == "tekst"
      additionalReplyData[1].label == "Intern reference"
      additionalReplyData[1].value == "tekst"
    }

    with(messageHeader.replyData[1]) {
      messageId == "MSG-12345"
      messageUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194c657")
      replyUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194c114")
      senderId == "1234567890"
      recipientId == "12345678"
      caseId == "SAG-4567"
      contactPointId == "12345678"
      generatingSystemId == "ABC-1234"
      comment == "tekst"
      additionalReplyData[0].label == "Intern note"
      additionalReplyData[0].value == "tekst"
      additionalReplyData[1].label == "Intern reference"
      additionalReplyData[1].value == "tekst"
      additionalReplyData[2].label == "Vedr"
      additionalReplyData[2].value == "tekst"
      additionalReplyData[3].label == "Att"
      additionalReplyData[3].value == "tekst"
    }
  }

  @Issue("AC-231")
  void "The MessageBody is parsed correctly"() {
    when:
    MessageBody messageBody = parsedMessage.messageBody

    then: "The MessageBody is parsed correctly"
    messageBody.createdDateTime == asLocalDateTime("2018-05-03T12:00:00Z")

    and: "The main document is parsed correctly"
    with(messageBody.mainDocument) {
      mainDocumentId == "456"
      label == "Tilbud om børnehaveplads"

      with(file[0]) {
        encodingFormat == "application/pdf"
        filename == "Pladsanvisning.pdf"
        language == "da"
        assertContent(content, "[embedded file]")
      }

      with(file[1]) {
        encodingFormat == "text/plain"
        filename == "Pladsanvisning.txt"
        language == "da"
        assertContent(content, "[embedded file]")
      }

      with(action[0]) {
        label == "Spørgeskema"
        startDateTime == asLocalDateTime("2018-11-09T12:00:00Z")
        endDateTime == asLocalDateTime("2018-12-09T12:00:00Z")
        entryPoint.url == "https://www.tusindfryd.dk/spørgeskema.html"
      }

      with(action[1]) {
        label == "Opret aftale i kalender"
        actionCode == "AFTALE"
        startDateTime == asLocalDateTime("2018-11-10T09:00:00Z")
        endDateTime == asLocalDateTime("2018-11-09T12:00:00Z")

        with(reservation as Reservation) {
          description == "Opstart Tusindfryd"
          reservationUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194d241")
          reservationAbstract == "Opstart"
          location == "Gl. Landevej 61, 9000 aalborg, Rød stue"
          startDateTime == asLocalDateTime("2018-11-10T09:00:00Z")
          endDateTime == asLocalDateTime("2018-11-10T12:00:00Z")
          organizerMail == "info@tusindfryd.dk"
          organizerName == "Jette Hansen"
        }
      }
    }

    and: "The first additional document is parsed correctly"
    with(messageBody.additionalDocument[0]) {
      additionalDocumentId == "789"
      label == "Tilbud om børnehaveplads"

      with(file[0]) {
        encodingFormat == "application/pdf"
        filename == "Pladsanvisning.pdf"
        language == "da"
        assertContent(content, "[embedded file]")
      }

      with(file[1]) {
        encodingFormat == "application/msword"
        filename == "Praktiske oplysninger.doc"
        language == "da"
        assertContent(content, "VGhpcyBpcyBhIHRlc3Q=")
      }

      with(action[0]) {
        label == "Tusindfryd hjemmeside"
        actionCode == "INFORMATION"
        startDateTime == asLocalDateTime("2018-11-10T09:00:00Z")
        endDateTime == asLocalDateTime("2018-11-09T12:00:00Z")
        entryPoint.url == "https://www.tusindfryd.dk"
      }

      with(action[1]) {
        label == "Opret aftale i kalender"
        actionCode == "AFTALE"

        with(reservation) {
          description == "Introduktion til nye forældre"
          reservationUUID == UUID.fromString("8c2ea15d-61fb-4ba9-9366-42f8b194e845")
          reservationAbstract == "Invitation"
          location == "Gl. Landevej 61, 9000 Aalborg"
          startDateTime == asLocalDateTime("2018-11-10T19:00:00Z")
          endDateTime == asLocalDateTime("2018-11-10T20:30:00Z")
          organizerMail == "info@tusindfryd.dk"
          organizerName == "Jette Hansen"
        }
      }
    }

    and: "The second additional document is parsed correctly"
    with(messageBody.additionalDocument[1]) {
      additionalDocumentId == "678"
      label == "Tilbud om børnehaveplads, vejledning"

      with(file[0]) {
        encodingFormat == "application/pdf"
        filename == "vejledning.pdf"
        language == "da"
        assertContent(content, "VGhpcyBpcyBhIHRlc3Q=")
      }

      with(action[0]) {
        label == "Register opslag"
        actionCode == "SELVBETJENING"
        startDateTime == asLocalDateTime("2018-11-10T09:00:00Z")
        endDateTime == asLocalDateTime("2018-11-09T12:00:00Z")
        entryPoint.url == "https://registration.nemhandel.dk/NemHandelRegisterWeb/public/participant/info?key=5798009811578&keytype=GLN"
      }
    }

    and: "The technical document is parsed correctly"
    with(messageBody.technicalDocument[0]) {
      technicalDocumentId == "222555"
      label == "Teknisk dokument"

      with(file[0]) {
        encodingFormat == "text/xml"
        filename == "TekniskDokument.xml"
        language == "da"
        assertContent(content, "VGhpcyBpcyBhIHRlc3Q=")
      }
    }
  }

  @Issue("AC-231")
  void "Parse full example of MeMo message - using UnstructuredAddress in Sender class instead of Address"() {
    given:
    File memo = new File("src/test/resources/xml/MeMo_Full_Example_V2.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)
    MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, true)

    when:
    Message message = parser.parse()

    then:
    "Parsing of UnstructuredAddress in AttentionData class" +
            "The hierarchy is: MessageHeader > Sender > AttentionData > UnstructuredAddress > attributes to parse"
    with(message.messageHeader.sender.attentionData.unstructuredAddress) {
      unstructured == "[Ustruktureret adresseangivlese]"
    }
  }

  @Issue("AC-231")
  void "Parse full example of MeMo message - using Address in Recipient class instead of Unstructured Address"() {
    given:
    File memo = new File("src/test/resources/xml/MeMo_Full_Example_V2.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)
    MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, false)

    when:
    Message message = parser.parse()
    Address address = message.messageHeader.recipient.attentionData.address

    then: "The Address is parsed correctly"
    with(address) {
      id == "8c2ea15d-61fb-4ba9-9366-42f8b194c852"
      addressLabel == "Gaden"
      houseNumber == "7A"
      door == "th"
      floor == "3"
      co == "C/O"
      zipCode == "9000"
      city == "Aalborg"
      country == "DK"

      with(addressPoint) {
        geographicEastingMeasure == "557501.23"
        geographicNorthingMeasure == "6336248.89"
        geographicHeightMeasure == "0.0"
      }
    }
  }

  @Issue("AC-231")
  void "Validate full example of MeMo message with invalid input"() {
    given: "A file variable memo is declared and initialize with an invalid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example_Invalid.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "A parser is declared and initialized"
    MeMoParser parser =
            MeMoParserFactory.createParser(fileInputStream, true)

    when: "Parsing fileInputSteam"
    parser.parse()

    then: "Exception must be thrown"
    thrown(Exception)
  }

  @Issue("AC-231")
  void "Validate full example of MeMo message with valid input"() {
    given: "A file variable memo is declared and initialize with a valid XML file"
    File memo = new File(XML_DIR + "MeMo_Full_Example.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "A parser is declared and initialized"
    MeMoParser parser =
            MeMoParserFactory.createParser(fileInputStream, true)

    when: "Parsing fileInputSteam"
    parser.parse()

    then: "No exceptions must be thrown"
    noExceptionThrown()
  }

  void assertContent(FileContent fileContent, String expectedContent) {
    fileContent.streamContent().getBytes() == expectedContent.getBytes()
  }

  private LocalDateTime asLocalDateTime(String utcTime) {
    return OffsetDateTime.parse(utcTime).toLocalDateTime()
  }

  void "New MeMo schema xsd can be validated "() {
    given:
    W3CSchemaFactory factory =
            (W3CSchemaFactory) XMLValidationSchemaFactory.newInstance(SCHEMA_ID_W3C_SCHEMA)

    when:
    XMLValidationSchema xmlValidationSchema = MeMoValidationSchemaProvider.getNewXmlSchema(factory)

    then:
    noExceptionThrown()
    xmlValidationSchema
  }

  void "New MeMo schema can be parsed"() {
    given:
    File memo = new File(XML_DIR + "MeMo_Full_Example_V1.2.xml")
    FileInputStream fileInputStream = new FileInputStream(memo)

    and: "A parser is declared and initialized"
    MeMoParser parser =
            MeMoParserFactory.createNewParser(fileInputStream, true)

    when: "Parsing fileInputSteam"
    parser.parse()

    then: "No exceptions must be thrown"
    noExceptionThrown()
  }
}
