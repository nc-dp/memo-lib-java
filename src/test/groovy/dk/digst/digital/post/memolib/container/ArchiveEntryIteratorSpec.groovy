package dk.digst.digital.post.memolib.container

import spock.lang.Specification

public class ArchiveEntryIteratorSpec extends Specification {

  void 'null inputstream is not allowed'() {
    when:
    new ArchiveEntryIterator(null)
    then:
    thrown(NullPointerException)
  }
}
