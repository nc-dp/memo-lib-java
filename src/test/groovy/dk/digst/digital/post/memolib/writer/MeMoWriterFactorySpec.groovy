package dk.digst.digital.post.memolib.writer

import spock.lang.Specification

class MeMoWriterFactorySpec extends Specification {

  void "the factory can create a writer"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when:
    MeMoStreamWriter writer = MeMoWriterFactory.createWriter(outputStream)

    then:
    writer != null
  }

  void "the factory can create a validating writer"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when:
    MeMoStreamWriter writer = MeMoWriterFactory.createWriter(outputStream)

    then:
    writer != null

  }

  void "the factory throws an exception for no output stream"() {
    when:
    MeMoWriterFactory.createWriter(null)

    then:
    thrown NullPointerException
  }

  void "the factory can create a writer with FileContentResolvers"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when: "registering the custom resolver"
    MeMoWriterFactory.registerFileContentResourceResolver(new CustomFileContentResolver())

    and: "creating the writer"
    MeMoStreamWriter writer = MeMoWriterFactory.createWriter(outputStream)

    then: "the writer contains the expected resolvers"
    List<FileContentResolver> resolvers = writer.fileContentLoader.resolvers
    resolvers.collect { it.getClass().getSimpleName() } == ["CustomFileContentResolver", "DefaultFileContentResolver"]
  }

  void "Adding a resolver twice does not modify the list of resolvers"() {
    given:
    OutputStream outputStream = new ByteArrayOutputStream()

    when: "registering the custom resolver once"
    MeMoWriterFactory.registerFileContentResourceResolver(new CustomFileContentResolver())

    and: "registering the custom resolver once more"
    MeMoWriterFactory.registerFileContentResourceResolver(new CustomFileContentResolver())

    and: "creating the writer"
    MeMoStreamWriter writer = MeMoWriterFactory.createWriter(outputStream)

    then: "the writer contains the expected resolvers"
    List<FileContentResolver> resolvers = writer.fileContentLoader.resolvers
    resolvers.collect { it.getClass().getSimpleName() } == ["CustomFileContentResolver", "DefaultFileContentResolver"]
  }

  private class CustomFileContentResolver implements FileContentResolver {
    @Override
    InputStream resolve(String location) throws IOException {
      return null
    }
  }

}
