package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MeMoValidationRuleFactory {

  public List<MeMoValidationRule> createRules(MeMoVersion meMoVersion) {
    if (meMoVersion.equals(MeMoVersion.MEMO_VERSION_NEW)) {
      return createNewMeMoRules();
    }
    return createRules();
  }

  /**
   * This utility creates rules used by MeMo validator for MeMo header validation.
   *
   * @return Unmodifiable Set of rules.
   */
  public List<MeMoValidationRule> createRules() {
    List<MeMoValidationRule> meMoMeMoValidationRules = new ArrayList<>();
    meMoMeMoValidationRules.add(new MeMoNotificationValidation());
    meMoMeMoValidationRules.add(new MeMoIdTypeValidation());
    meMoMeMoValidationRules.add(new MeMoPostTypeValidation());
    meMoMeMoValidationRules.add(new MeMoCprValidation());
    meMoMeMoValidationRules.add(new MeMoCvrValidation());
    meMoMeMoValidationRules.add(new MeMoEmptyBodyValidation());

    return Collections.unmodifiableList(meMoMeMoValidationRules);
  }

  public List<MeMoValidationRule> createNewMeMoRules() {
    List<MeMoValidationRule> meMoMeMoValidationRules = new ArrayList<>();
    meMoMeMoValidationRules.add(new MeMoNotificationValidation());
    meMoMeMoValidationRules.add(new MeMoIdTypeValidation());
    meMoMeMoValidationRules.add(new MeMoReplyDataValidation());
    meMoMeMoValidationRules.add(new MeMoPostTypeValidation());
    meMoMeMoValidationRules.add(new MeMoCprValidation());
    meMoMeMoValidationRules.add(new MeMoCvrValidation());
    meMoMeMoValidationRules.add(new MeMoEmptyBodyValidation());

    return Collections.unmodifiableList(meMoMeMoValidationRules);
  }
}
