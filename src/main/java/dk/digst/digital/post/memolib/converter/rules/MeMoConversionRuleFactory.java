package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MeMoConversionRuleFactory {

  /**
   * This utility creates rules used by MeMo conversions.
   *
   * @param sourceVersion version of the original MeMo.
   * @return Unmodifiable Set of rules.
   */
  public List<MeMoConversionRule> createRules(MeMoVersion sourceVersion) {
    List<MeMoConversionRule> meMoConversionRules = new ArrayList<>();
    if (sourceVersion == MeMoVersion.MEMO_VERSION_OLD) {
      meMoConversionRules.add(new UpConvertVersionRule());
      meMoConversionRules.add(new UpConvertReplyDataRule());
    } else if (sourceVersion == MeMoVersion.MEMO_VERSION_NEW) {
      meMoConversionRules.add(new DownConvertHeaderRule());
      meMoConversionRules.add(new DownConvertVersionRule());
    }
    return Collections.unmodifiableList(meMoConversionRules);
  }
}
