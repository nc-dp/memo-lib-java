package dk.digst.digital.post.memolib.xml.schema;

import com.ctc.wstx.exc.WstxIOException;
import com.ctc.wstx.msv.W3CSchemaFactory;
import dk.digst.digital.post.memolib.config.GlobalConfig;
import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.xml.stax.Stax2FactoryProvider;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import javax.xml.stream.XMLStreamException;
import lombok.experimental.UtilityClass;
import org.codehaus.stax2.validation.XMLValidationSchema;
import org.codehaus.stax2.validation.XMLValidationSchemaFactory;
import org.xml.sax.InputSource;

@UtilityClass
public class MeMoValidationSchemaProvider {

  /**
   * Provides an XML schema for MeMo message validation
   *
   * @return XMLValidationSchema
   */
  public static XMLValidationSchema getMeMoValidationSchema() throws XMLStreamException {
    return getMeMoValidationSchema(MeMoVersion.MEMO_VERSION_OLD);
  }

  public static XMLValidationSchema getMeMoValidationSchema(MeMoVersion targetVersion)
      throws XMLStreamException {
    if (GlobalConfig.isSkipBase64Binary()) {
      return getXmlValidationSchemaSkipBase64Binary(targetVersion);
    } else {
      return getXmlValidationSchema(targetVersion);
    }
  }

  private static XMLValidationSchema getXmlValidationSchema(MeMoVersion memoVersion)
      throws XMLStreamException {
    XMLValidationSchema xmlValidationSchema;
    W3CSchemaFactory factory =
        (W3CSchemaFactory)
            XMLValidationSchemaFactory.newInstance(XMLValidationSchema.SCHEMA_ID_W3C_SCHEMA);
    if (isMeMoVersionNew(memoVersion)) {
      xmlValidationSchema = getNewXmlSchema(factory);
    } else {
      xmlValidationSchema = getXmlSchema(factory);
    }
    return xmlValidationSchema;
  }

  private static XMLValidationSchema getXmlValidationSchemaSkipBase64Binary(MeMoVersion memoVersion)
      throws XMLStreamException {
    XMLValidationSchema xmlValidationSchemaSkipBase64Binary;
    W3CSkipBase64SchemaFactory factory = new W3CSkipBase64SchemaFactory();

    if (isMeMoVersionNew(memoVersion)) {
      xmlValidationSchemaSkipBase64Binary = getNewXmlSchema(factory);
    } else {
      xmlValidationSchemaSkipBase64Binary = getXmlSchema(factory);
    }

    return xmlValidationSchemaSkipBase64Binary;
  }

  private static boolean isMeMoVersionNew(MeMoVersion memoVersion) {
    return memoVersion == MeMoVersion.MEMO_VERSION_NEW;
  }

  public XMLValidationSchema getXmlSchema(W3CSchemaFactory factory) throws XMLStreamException {
    return factory.createSchema(Stax2FactoryProvider.class.getResource(GlobalConfig.MEMO_CORE_XSD));
  }

  public XMLValidationSchema getNewXmlSchema(W3CSchemaFactory factory) throws XMLStreamException {
    return factory.createSchema(
        Stax2FactoryProvider.class.getResource(GlobalConfig.MEMO_CORE_XSD_NEW_VERSION));
  }

  private static class W3CSkipBase64SchemaFactory extends W3CSchemaFactory {

    @Override
    public XMLValidationSchema createSchema(URL url) throws XMLStreamException {
      try (InputStream stream = url.openStream()) {
        String xsd =
            new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        String skipBase64Xsd = xsd.replace("xs:base64Binary", "xs:anyType");
        InputSource src =
            new InputSource(
                new ByteArrayInputStream(skipBase64Xsd.getBytes(StandardCharsets.UTF_8)));
        src.setSystemId(url.toExternalForm());
        return loadSchema(src, url);
      } catch (IOException ioe) {
        throw new WstxIOException(ioe);
      }
    }
  }
}
