package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import java.util.Set;

public interface MeMoValidationRule {
  void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors);
}
