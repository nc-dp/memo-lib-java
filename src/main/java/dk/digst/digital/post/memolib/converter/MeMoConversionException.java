package dk.digst.digital.post.memolib.converter;

public class MeMoConversionException extends RuntimeException {

  public MeMoConversionException(String message) {
    super(message);
  }

  public MeMoConversionException(Exception e) {
    super(e);
  }
}
