package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;

public class DownConvertVersionRule implements MeMoConversionRule {

  @Override
  public void convert(Message source) {
    source.setMemoSchVersion(MeMoVersion.MEMO_SCH_VERSION);
    source.setMemoVersion(MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber());
  }
}
