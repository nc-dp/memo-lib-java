package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.Recipient;
import dk.digst.digital.post.memolib.model.Sender;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Set;
import java.util.regex.Pattern;

public class MeMoCprValidation implements MeMoValidationRule {

  private static final String CPR = "CPR";

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (message.getMessageHeader().getSender() != null) {
      validateSenderCpr(message.getMessageHeader().getSender(), meMoValidatorErrors);
    }
    if (message.getMessageHeader().getRecipient() != null) {
      validateRecipientCpr(message.getMessageHeader().getRecipient(), meMoValidatorErrors);
    }
  }

  private boolean isCpr(String idType) {
    return CPR.equalsIgnoreCase(idType);
  }

  private static void validateCpr(
      String cpr, ValidationErrorCode errorCode, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!Pattern.matches("^\\d{6}-?\\d{4}$", cpr)) {
      meMoValidatorErrors.add(new MeMoValidatorError(errorCode.errorCode(), cpr));
    }
  }

  private void validateSenderCpr(Sender sender, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (isCpr(sender.getIdType())) {
      validateCpr(
          sender.getSenderId(), ValidationErrorCode.SENDER_CPR_INVALID, meMoValidatorErrors);
    }

    if (sender.getRepresentative() != null && isCpr(sender.getRepresentative().getIdType())) {
      validateCpr(
          sender.getRepresentative().getRepresentativeId(),
          ValidationErrorCode.REPRESENTATIVE_CPR_INVALID,
          meMoValidatorErrors);
    }
  }

  private void validateRecipientCpr(
      Recipient recipient, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (isCpr(recipient.getIdType())) {
      validateCpr(
          recipient.getRecipientId(),
          ValidationErrorCode.RECIPIENT_CPR_INVALID,
          meMoValidatorErrors);
    }
  }
}
