package dk.digst.digital.post.memolib.config;

import dk.digst.digital.post.memolib.xml.stax.Stax2FactoryProvider;
import javax.xml.stream.XMLInputFactory;
import lombok.experimental.UtilityClass;
import org.codehaus.stax2.XMLInputFactory2;

@UtilityClass
public class GlobalConfig {

  public final String MEMO_CORE_XSD = "/schemas/MeMo_core.xsd";

  public final String MEMO_CORE_XSD_NEW_VERSION = "/schemas/Version1.2/MeMo_core.xsd";

  public final Configurable configurableValues = new Configurable(2500);

  private boolean skipBase64Binary = false;

  /**
   * Default: true
   *
   * @see javax.xml.stream.XMLInputFactory#IS_COALESCING
   */
  public void setCoalescing(boolean coalescing) {
    Stax2FactoryProvider.getXmlInputFactory()
        .setProperty(XMLInputFactory.IS_COALESCING, coalescing);
  }

  /**
   * Default: true
   *
   * @see org.codehaus.stax2.XMLInputFactory2#P_PRESERVE_LOCATION
   */
  public void setPreserveLocation(boolean preserveLocation) {
    Stax2FactoryProvider.getXmlInputFactory()
        .setProperty(XMLInputFactory2.P_PRESERVE_LOCATION, preserveLocation);
  }

  /**
   * @see org.codehaus.stax2.XMLInputFactory2#configureForLowMemUsage()
   */
  public void configureForLowMemUsage() {
    Stax2FactoryProvider.getXmlInputFactory().configureForLowMemUsage();
  }

  /**
   * @see org.codehaus.stax2.XMLInputFactory2#configureForSpeed()
   */
  public void configureForSpeed() {
    Stax2FactoryProvider.getXmlInputFactory().configureForSpeed();
  }

  public boolean isSkipBase64Binary() {
    return GlobalConfig.skipBase64Binary;
  }

  /**
   * Default: false Parser that skips base64Binary for performance reasons. This will not provide
   * proper validation but can be useful in some scenarios, if base64Binary is validated elsewhere.
   */
  public void setSkipBase64Binary(boolean skipBase64Binary) {
    GlobalConfig.skipBase64Binary = skipBase64Binary;
  }
}
