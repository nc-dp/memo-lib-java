package dk.digst.digital.post.memolib.util;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.writer.MeMoStreamWriter;
import dk.digst.digital.post.memolib.writer.MeMoWriteException;
import dk.digst.digital.post.memolib.writer.MeMoWriterFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MeMoStreamUtils {

  public InputStream getMessageAsInputStream(Message message) throws IOException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    try (MeMoStreamWriter writer = getWriter(message.getMemoVersion(), outputStream)) {
      try {
        writer.write(message);
      } catch (MeMoWriteException e) {
        throw new IOException(e);
      }
    }
    return new ByteArrayInputStream(outputStream.toByteArray());
  }

  private MeMoStreamWriter getWriter(BigDecimal version, OutputStream outputStream) {
    if (MeMoVersion.MEMO_VERSION_NEW.test(version)) {
      return MeMoWriterFactory.createNewWriter(outputStream);
    } else if (MeMoVersion.MEMO_VERSION_OLD.test(version)) {
      return MeMoWriterFactory.createWriter(outputStream);
    }

    throw new IllegalArgumentException("Invalid version provided");
  }
}
