package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.model.AttentionData;
import dk.digst.digital.post.memolib.model.ContentResponsible;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.model.MessageType;
import dk.digst.digital.post.memolib.model.Representative;
import dk.digst.digital.post.memolib.model.Sender;
import org.apache.commons.lang3.StringUtils;

public class DownConvertHeaderRule implements MeMoConversionRule {

  private MessageHeader downConvertHeader(MessageHeader header) {
    if (header.getLegalNotification() == null) {
      header.setLegalNotification(false);
    }

    if (header.getMandatory() == null) {
      header.setMandatory(false);
    }

    if (notificationIsTooLong(header)) {
      header.setNotification(StringUtils.abbreviate(header.getNotification(), 150));
    }

    if (representativeIsSet(header)) {
      header.setSender(enrichSenderAttentionData(header.getSender()));
    }

    if (forwardDataIsSetWithRepresentative(header)) {
      header.getForwardData().setOriginalContentResponsible(header.getForwardData().getOriginalRepresentative());
      header.getForwardData().setOriginalRepresentative(null);
    }

    return header;
  }

  private boolean forwardDataIsSetWithRepresentative(MessageHeader header) {
    return header.getForwardData() != null
        && header.getForwardData().getOriginalRepresentative() != null;
  }

  private Sender enrichSenderAttentionData(Sender sender) {
    AttentionData attentionData = sender.getAttentionData();
    if (attentionData == null) {
      attentionData = new AttentionData();
      sender.setAttentionData(attentionData);
    }
    Representative representative = sender.getRepresentative();
    attentionData.setContentResponsible(
        new ContentResponsible(representative.getRepresentativeId(), representative.getLabel()));

    sender.setRepresentative(null);
    return sender;
  }

  private boolean representativeIsSet(MessageHeader entry) {
    return entry.getSender() != null && entry.getSender().getRepresentative() != null;
  }

  private boolean notificationIsTooLong(MessageHeader entry) {
    return entry.getNotification() != null
        && entry.getNotification().length() > 150
        && entry.getMessageType() == MessageType.DIGITALPOST;
  }

  @Override
  public void convert(Message source) {
    source.setMessageHeader(downConvertHeader(source.getMessageHeader()));
  }
}
