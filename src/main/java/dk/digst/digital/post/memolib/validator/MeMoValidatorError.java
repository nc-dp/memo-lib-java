package dk.digst.digital.post.memolib.validator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MeMoValidatorError {
  private String errorCode;
  private String[] args;

  public MeMoValidatorError(String errorCode) {
    this.errorCode = errorCode;
    this.args = new String[0];
  }

  public MeMoValidatorError(String errorCode, String arg) {
    this.errorCode = errorCode;
    this.args = new String[] {arg};
  }
}
