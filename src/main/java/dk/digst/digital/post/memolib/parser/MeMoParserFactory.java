package dk.digst.digital.post.memolib.parser;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.xml.parser.MeMoXmlStreamParserFactory;
import java.io.InputStream;
import javax.xml.stream.XMLStreamException;

/**
 * The MeMoParserFactory is used to create a parser which can parse a representation of a MeMo
 * message.
 */
public class MeMoParserFactory {

  private MeMoParserFactory() {}

  /**
   * Creates a parser for memo messages using the latest schema version.
   * <p>
   * This method differs from the basic {@link #createParser} method by allowing the use of the latest
   * memo version schema and providing an option to enable or disable schema validation through a boolean flag.
   *
   * @param inputStream the input stream containing the memo message
   * @param enableValidation a boolean flag indicating whether schema validation should be enabled
   * @return an instance of {@link MeMoParser}
   * @throws MeMoParseException if an error occurs during parsing
   */
  public static MeMoParser createNewParser(InputStream inputStream, boolean enableValidation)
      throws MeMoParseException {
    return createParser(inputStream, enableValidation, MeMoVersion.MEMO_VERSION_NEW);
  }

  /**
   * Creates a basic non-validating parser for old memo messages.
   *
   * @param inputStream the input stream containing the memo message
   * @return an instance of {@link MeMoParser}
   * @throws MeMoParseException if an error occurs during parsing
   */
  public static MeMoParser createParser(InputStream inputStream) throws MeMoParseException {
    return createParser(inputStream, false, MeMoVersion.MEMO_VERSION_OLD);
  }


  /**
   * Creates a validatable parser for old memo messages.
   *
   * @param inputStream the input stream containing the memo message
   * @param enableValidation a boolean flag indicating whether schema validation should be enabled
   * @return an instance of {@link MeMoParser}
   * @throws MeMoParseException if an error occurs during parsing
   */
  public static MeMoParser createParser(InputStream inputStream, boolean enableValidation) throws MeMoParseException {
    return createParser(inputStream, enableValidation, MeMoVersion.MEMO_VERSION_OLD);
  }

  /**
   * Creates a validating parser for memo messages.
   * <p>
   * This method differs from {@link #createParser} in that it accepts a boolean flag to enable schema validation
   * and specifies the target version of the schema to use.
   *
   * @param inputStream the input stream containing the memo message
   * @param enableValidation a boolean flag indicating whether schema validation should be enabled
   * @param targetVersion the target version of the memo schema to use if validation is enabled
   * @return an instance of {@link MeMoParser}
   * @throws MeMoParseException if an error occurs during parsing
   */
  public static MeMoParser createParser(
      InputStream inputStream, boolean enableValidation, MeMoVersion targetVersion)
      throws MeMoParseException {

    try {
      return MeMoXmlStreamParserFactory.createParser(inputStream, enableValidation, targetVersion);
    } catch (XMLStreamException e) {
      throw new MeMoParseException(e);
    }
  }
}
