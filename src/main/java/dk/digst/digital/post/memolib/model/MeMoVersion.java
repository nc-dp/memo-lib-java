package dk.digst.digital.post.memolib.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MeMoVersion implements Predicate<BigDecimal> {
  MEMO_VERSION_OLD(BigDecimal.valueOf(1.1)),
  MEMO_VERSION_NEW(BigDecimal.valueOf(1.2)),
  UNKNOWN(null);
  /**
   * @deprecated This constant will be removed in 2026 due to the integration of schematron rules
   *     into the digital post solution. Therefore, the schema version number will no longer be
   *     supported.
   */
  @Deprecated public static final String MEMO_SCH_VERSION = "1.1.0";

  private BigDecimal memoVersionNumber;

  public static MeMoVersion fromValue(String version) {
    if (version == null){
      return  UNKNOWN;
    }
    BigDecimal versionNumber = BigDecimal.valueOf(Double.parseDouble(version));
    return fromValue(versionNumber);
  }

  public static MeMoVersion fromValue(BigDecimal version) {
    MeMoVersion meMoVersion =
        Arrays.stream(MeMoVersion.values())
            .filter(it -> it.test(version))
            .findFirst()
            .orElse(UNKNOWN);

    if (meMoVersion == UNKNOWN) {
      meMoVersion.setMemoVersionNumber(version);
    }
    return meMoVersion;
  }

  private void setMemoVersionNumber(BigDecimal number) {
    memoVersionNumber = number;
  }

  @Override
  public boolean test(BigDecimal memoVersion) {
    return Objects.equals(this.memoVersionNumber, memoVersion);
  }

  public boolean test(String memoVersion) {
    return Objects.equals(this.memoVersionNumber.toString(), memoVersion);
  }
}
