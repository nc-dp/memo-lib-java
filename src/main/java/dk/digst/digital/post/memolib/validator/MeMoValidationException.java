package dk.digst.digital.post.memolib.validator;

public class MeMoValidationException extends RuntimeException {

  public MeMoValidationException(String message) {
    super(message);
  }

  public MeMoValidationException(Exception e) {
    super(e);
  }
}
