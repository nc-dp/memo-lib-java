package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageType;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class MeMoNotificationValidation implements MeMoValidationRule {

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!isMessageTypeNemSms(message.getMessageHeader().getMessageType())) {
      return;
    }

    if (StringUtils.isBlank(message.getMessageHeader().getNotification())) {
      meMoValidatorErrors.add(
          new MeMoValidatorError(ValidationErrorCode.EMPTY_NOTIFICATION_NOT_ALLOWED.errorCode()));
    }
  }


  private boolean isMessageTypeNemSms(MessageType messageType) {
    return  messageType == MessageType.NEMSMS;
  }
}
