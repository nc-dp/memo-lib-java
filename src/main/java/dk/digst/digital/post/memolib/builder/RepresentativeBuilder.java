package dk.digst.digital.post.memolib.builder;

import dk.digst.digital.post.memolib.model.Representative;

public class RepresentativeBuilder {

    private String representativeID;
    private String idType;
    private String idTypeLabel;
    private String label;


    public static RepresentativeBuilder newBuilder() {
        return new RepresentativeBuilder();
    }

    public RepresentativeBuilder representativeID(String representativeID) {
        this.representativeID = representativeID;
        return this;
    }

    public RepresentativeBuilder idType(String idType) {
        this.idType = idType;
        return this;
    }

    public RepresentativeBuilder idTypeLabel(String idTypeLabel) {
        this.idTypeLabel = idTypeLabel;
        return this;
    }

    public RepresentativeBuilder label(String label) {
        this.label = label;
        return this;
    }

    public Representative build() {
        return new Representative(representativeID, idType, idTypeLabel, label);
    }
}
