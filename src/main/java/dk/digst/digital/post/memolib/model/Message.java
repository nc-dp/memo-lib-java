package dk.digst.digital.post.memolib.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Message implements MeMoClass {

  @JacksonXmlProperty(isAttribute = true)
  private BigDecimal memoVersion = MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber();

  @JacksonXmlProperty(isAttribute = true)
  private String memoSchVersion = null;

  @JacksonXmlProperty(localName = "MessageHeader")
  private MessageHeader messageHeader;

  @JacksonXmlProperty(localName = "MessageBody")
  private MessageBody messageBody;
}
