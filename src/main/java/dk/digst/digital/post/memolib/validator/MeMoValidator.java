package dk.digst.digital.post.memolib.validator;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.validator.rules.MeMoValidationRule;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MeMoValidator {
  private final List<MeMoValidationRule> validationRules;
  private final Message message;

  public MeMoValidator(
      List<MeMoValidationRule> validationRules,
      Message message) {
    this.validationRules = validationRules;
    this.message = message;
  }

  public Set<MeMoValidatorError> validate() {
    if (message == null) {
      throw new MeMoValidationException("Message is null. Can't validate.");
    }
    return validate(message);
  }

  public Set<MeMoValidatorError> validate(Message message) {
    Set<MeMoValidatorError> meMoValidatorErrors = new HashSet<>();

    for (MeMoValidationRule rule : validationRules) {
      rule.validate(message, meMoValidatorErrors);
    }

    return meMoValidatorErrors;
  }
}
