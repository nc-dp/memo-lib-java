package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;

public class UpConvertVersionRule implements MeMoConversionRule {
  @Override
  public void convert(Message source) {
    source.setMemoSchVersion(null);
    source.setMemoVersion(MeMoVersion.MEMO_VERSION_NEW.getMemoVersionNumber());
  }
}
