package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.Recipient;
import dk.digst.digital.post.memolib.model.Sender;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Set;
import java.util.regex.Pattern;

public class MeMoCvrValidation implements MeMoValidationRule {

  private static final String CVR = "CVR";

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (message.getMessageHeader().getSender() != null) {
      validateSenderCvr(message.getMessageHeader().getSender(), meMoValidatorErrors);
    }
    if (message.getMessageHeader().getRecipient() != null) {
      validateRecipientCvr(message.getMessageHeader().getRecipient(), meMoValidatorErrors);
    }
  }

  private boolean isCvr(String idType) {
    return CVR.equalsIgnoreCase(idType);
  }

  private static void validateCvr(
      String cvr, ValidationErrorCode errorCode, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!Pattern.matches("^\\d{8}$", cvr)) {
      meMoValidatorErrors.add(new MeMoValidatorError(errorCode.errorCode(), cvr));
    }
  }

  private void validateSenderCvr(Sender sender, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (isCvr(sender.getIdType())) {
      validateCvr(
          sender.getSenderId(), ValidationErrorCode.SENDER_CVR_INVALID, meMoValidatorErrors);
    }

    if (sender.getRepresentative() != null && isCvr(sender.getRepresentative().getIdType())) {
      validateCvr(
          sender.getRepresentative().getRepresentativeId(),
          ValidationErrorCode.REPRESENTATIVE_CVR_INVALID,
          meMoValidatorErrors);
    }
  }

  private void validateRecipientCvr(
      Recipient recipient, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (isCvr(recipient.getIdType())) {
      validateCvr(
          recipient.getRecipientId(),
          ValidationErrorCode.RECIPIENT_CVR_INVALID,
          meMoValidatorErrors);
    }
  }
}
