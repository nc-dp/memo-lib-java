package dk.digst.digital.post.memolib.converter;

import dk.digst.digital.post.memolib.converter.rules.MeMoConversionRule;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.parser.MeMoParseException;
import dk.digst.digital.post.memolib.parser.MeMoParser;
import dk.digst.digital.post.memolib.util.MeMoStreamUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MeMoConverter implements Closeable {
  private final List<MeMoConversionRule> conversionRules;
  private final MeMoParser parser;

  public MeMoConverter(List<MeMoConversionRule> conversionRules, MeMoParser parser) {
    this.conversionRules = conversionRules;
    this.parser = parser;
  }

  public Message parseIntoConvertedObject() throws MeMoConversionException, IOException {
    try {
      Message parsedMessage = parser.parse();
      for (MeMoConversionRule rule : conversionRules) {
        rule.convert(parsedMessage);
      }
      return parsedMessage;
    } catch (MeMoParseException | IOException e) {
      throw new MeMoConversionException(e);
    } finally {
      close();
    }
  }

  public InputStream parseIntoConvertedStream() throws MeMoConversionException, IOException {
    return MeMoStreamUtils.getMessageAsInputStream(parseIntoConvertedObject());
  }

  @Override
  public void close() throws IOException {
    parser.close();
  }
}
