package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageBody;
import dk.digst.digital.post.memolib.model.MessageType;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Set;

public class MeMoEmptyBodyValidation implements MeMoValidationRule {

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (isMessageTypeDigitalPost(message.getMessageHeader().getMessageType())
        && isMessageBodyNullOrEmpty(message.getMessageBody())) {
      meMoValidatorErrors.add(
          new MeMoValidatorError(ValidationErrorCode.MESSAGE_BODY_NOT_FOUND.errorCode()));
    }
  }

  private boolean isMessageBodyNullOrEmpty(MessageBody messageBody) {
    return messageBody == null || hasNoDocuments(messageBody);
  }

  private boolean isMessageTypeDigitalPost(MessageType messageType) {
    return messageType == MessageType.DIGITALPOST;
  }

  private boolean hasNoDocuments(MessageBody messageBody) {
    boolean isAdditionalDocumentsEmpty =
        messageBody.getAdditionalDocument() == null
            || messageBody.getAdditionalDocument().isEmpty();
    boolean isTechnicalDocumentsEmpty =
        messageBody.getTechnicalDocument() == null || messageBody.getTechnicalDocument().isEmpty();

    return (messageBody.getMainDocument() == null
        && isAdditionalDocumentsEmpty
        && isTechnicalDocumentsEmpty);
  }
}
