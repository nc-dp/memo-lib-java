package dk.digst.digital.post.memolib.writer;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.xml.writer.MeMoXmlWriterFactory;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;

/** MeMoWriterFactory can be used to create an instance of a {@link MeMoStreamWriter}. */
public class MeMoWriterFactory {

  private static final List<FileContentResolver> FILE_CONTENT_RESOLVERS = new ArrayList<>();

  /* private constructor to prevent instantiation of the factory */
  private MeMoWriterFactory() {}

  public static void clearResourceResolvers() {
    FILE_CONTENT_RESOLVERS.clear();
  }

  public static void registerFileContentResourceResolver(FileContentResolver resolver) {
    if (FILE_CONTENT_RESOLVERS.stream()
        .noneMatch(
            registeredResolver -> registeredResolver.getClass().equals(resolver.getClass()))) {
      FILE_CONTENT_RESOLVERS.add(resolver);
    }
  }

  /**
   * This method creates a {@link MeMoStreamWriter} which will use the provided {@link OutputStream}
   * to write the MeMo message. The message will be in the old version.
   *
   * @param outputStream the stream to be written to
   * @return MeMoStreamWriter
   * @throws UncheckedIOException if a low-level I/O problem occurs
   */
  public static MeMoStreamWriter createWriter(@NonNull OutputStream outputStream) {
    return createWriter(outputStream, false, MeMoVersion.MEMO_VERSION_OLD);
  }

  /**
   * This method creates a {@link MeMoStreamWriter} which will use the provided {@link OutputStream}
   * to write the MeMo message. In addition the method signature accepts boolean flag to indicate
   * whether validation should be performed on the old version.
   *
   * @param outputStream the stream to be written to
   * @return MeMoStreamWriter
   * @throws UncheckedIOException if a low-level I/O problem occurs
   */
  public static MeMoStreamWriter createWriter(
      @NonNull OutputStream outputStream, boolean enableValidation) {
    return createWriter(outputStream, enableValidation, MeMoVersion.MEMO_VERSION_OLD);
  }
  /**
   * This method creates a {@link MeMoStreamWriter} which will use the provided {@link OutputStream}
   * to write the MeMo message. In addition the method signature accepts boolean flag to indicate
   * whether validation should be performed and on what version from the provided target version
   *
   * @param outputStream the stream to be written to
   * @param enableValidation if true MeMo will be validated upon writing
   * @param targetVersion the version the memo should be written too
   * @return MeMoStreamWriter
   * @throws UncheckedIOException if a low-level I/O problem occurs
   */
  public static MeMoStreamWriter createWriter(
      @NonNull OutputStream outputStream, boolean enableValidation, MeMoVersion targetVersion) {

    List<FileContentResolver> resolvers = new ArrayList<>(FILE_CONTENT_RESOLVERS);
    resolvers.add(new DefaultFileContentResolver());
    FileContentLoader fileContentLoader = new FileContentLoader(resolvers);

    try {
      return MeMoXmlWriterFactory.createWriter(
          outputStream, fileContentLoader, enableValidation, targetVersion);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  /**
   * This method creates a {@link MeMoStreamWriter} which will use the provided {@link OutputStream}
   * to write the MeMo message as the newest version of MeMo.
   *
   * @param outputStream the stream to be written to
   * @return MeMoStreamWriter
   * @throws UncheckedIOException if a low-level I/O problem occurs
   */
  public static MeMoStreamWriter createNewWriter(@NonNull OutputStream outputStream) {
    return createNewWriter(outputStream, false);
  }

  /**
   * This method creates a {@link MeMoStreamWriter} which will use the provided {@link OutputStream}
   * to write the MeMo message. In addition the method signature accepts boolean flag to indicate
   * whether validation should be performed on the newest version.
   *
   * @param outputStream the stream to be written to
   * @return MeMoStreamWriter
   * @throws UncheckedIOException if a low-level I/O problem occurs
   */
  public static MeMoStreamWriter createNewWriter(
      OutputStream outputStream, Boolean enableValidation) {
    return createWriter(outputStream, enableValidation, MeMoVersion.MEMO_VERSION_NEW);
  }
}
