package dk.digst.digital.post.memolib.builder;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageBody;
import dk.digst.digital.post.memolib.model.MessageHeader;
import java.math.BigDecimal;

public class MessageBuilder {

  private MessageHeader messageHeader;

  private MessageBody messageBody;

  private BigDecimal memoVersion = MeMoVersion.MEMO_VERSION_OLD.getMemoVersionNumber();

  private String memoSchVersion = MeMoVersion.MEMO_SCH_VERSION;

  public static MessageBuilder newBuilder() {
    return new MessageBuilder();
  }

  public MessageBuilder messageHeader(MessageHeader messageHeader) {
    this.messageHeader = messageHeader;
    return this;
  }

  public MessageBuilder messageBody(MessageBody messageBody) {
    this.messageBody = messageBody;
    return this;
  }

  public MessageBuilder memoVersion(BigDecimal memoVersion) {
    this.memoVersion = memoVersion;
    return this;
  }

  public MessageBuilder memoVersion(MeMoVersion memoVersion) {
    this.memoVersion = memoVersion.getMemoVersionNumber();
    return this;
  }

  public MessageBuilder memoSchVersion(String memoSchVersion) {
    this.memoSchVersion = memoSchVersion;
    return this;
  }

  public Message build() {
    return new Message(memoVersion, memoSchVersion, messageHeader, messageBody);
  }
}
