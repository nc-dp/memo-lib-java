package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.builder.ReplyDataBuilder;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.model.ReplyData;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class UpConvertReplyDataRule implements MeMoConversionRule {

  @Override
  public void convert(Message source) {
    if (isReplyAndReplyDataEmpty(source.getMessageHeader())) {
      source.getMessageHeader().setReplyData(createReplyData(source.getMessageHeader()));
    }
  }

  private boolean isReplyAndReplyDataEmpty(MessageHeader messageHeader) {
    if (messageHeader.getReply() == null || !messageHeader.getReply()) {
      return false;
    }

    return CollectionUtils.isEmpty(messageHeader.getReplyData());
  }

  private List<ReplyData> createReplyData(MessageHeader messageHeader) {
    ReplyData replyData =
        ReplyDataBuilder.newBuilder().messageUUID(messageHeader.getMessageUUID()).build();

    List<ReplyData> replyDataList = new ArrayList<>();
    replyDataList.add(replyData);

    return replyDataList;
  }
}
