package dk.digst.digital.post.memolib.util;

import dk.digst.digital.post.memolib.config.GlobalConfig;
import dk.digst.digital.post.memolib.model.MeMoVersion;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.NonNull;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ReadMeMoVersion {

  /**
   * Reads the memo version from the beginning of an input stream.
   * This method captures an input stream and reads the initial portion into a buffer. It does not
   * close the original stream and will reset it to its initial position after reading.
   *
   * @param inputStream the buffered input stream containing the memo message
   * @return the {@link MeMoVersion} extracted from the memo message, or {@code null} if the version cannot be found
   * @throws IOException if a low lever error occurs while reading the stream
   */
  @NonNull
  public MeMoVersion getMeMoVersion(BufferedInputStream inputStream) throws IOException {
    String result = readMeMoHead(inputStream);
    Pattern pattern = Pattern.compile("memoVersion=[\"'](\\d+\\.\\d+)[\"']");
    Matcher matcher = pattern.matcher(result);
    if (matcher.find()) {
      return MeMoVersion.fromValue(matcher.group(1));
    }
    return MeMoVersion.UNKNOWN;
  }

  private String readMeMoHead(BufferedInputStream inputStream) throws IOException {
    StringBuilder sb = new StringBuilder();

    int length = GlobalConfig.configurableValues.getVersionReaderWindowSize();
    byte[] buffer = new byte[length];
    inputStream.mark(length + 1);
    int readAmount = inputStream.read(buffer, 0, length);
    inputStream.reset();

    if (readAmount > 0) {
      sb.append(new String(buffer, StandardCharsets.UTF_8));
    }
    return sb.toString();
  }
}
