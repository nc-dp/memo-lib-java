package dk.digst.digital.post.memolib.validator;

import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.validator.rules.MeMoValidationRuleFactory;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MeMoValidatorFactory {
  /**
   * Create a Message MeMo validator specified by the MeMo version
   *
   * @param meMoVersion the target version of the MeMo to use for validation
   * @return Memo validator
   */
  public static MeMoValidator createMeMoValidator(MeMoVersion meMoVersion) {
    return new MeMoValidator(
        MeMoValidationRuleFactory.createRules(meMoVersion),
        null);
  }

  /**
   * Create a Message MeMo validator with a Message
   *
   * @param message the Message that can be validated
   * @return Memo validator
   */
  public static MeMoValidator createMeMoValidator(Message message) {
    return new MeMoValidator(
            MeMoValidationRuleFactory.createRules(MeMoVersion.fromValue(message.getMemoVersion())),
            message);
  }
}
