package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.MessageHeader;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class MeMoPostTypeValidation implements MeMoValidationRule {

  private final List<String> validPostType = Arrays.asList("MYNDIGHEDSPOST", "VIRKSOMHEDSPOST");

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!postTypeExists(message.getMessageHeader())) {
      return;
    }

    if (!validPostType.contains(message.getMessageHeader().getPostType())) {
      meMoValidatorErrors.add(
          new MeMoValidatorError(
              ValidationErrorCode.POST_TYPE_INVALID.errorCode(),
              message.getMessageHeader().getPostType()));
    }
  }

  private boolean postTypeExists(MessageHeader messageHeader) {
    return messageHeader.getPostType() != null;
  }
}
