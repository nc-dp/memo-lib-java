package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.ReplyData;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.List;
import java.util.Set;

public class MeMoReplyDataValidation implements MeMoValidationRule {

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!hasReplyAndReplyIsTrue(message.getMessageHeader().getReply())) {
      return;
    }

    validateReplyData(message.getMessageHeader().getReplyData(), meMoValidatorErrors);
  }

  private boolean hasReplyAndReplyIsTrue(Boolean reply) {
    return Boolean.TRUE.equals(reply);
  }

  private void validateReplyData(
      List<ReplyData> replyDataList, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (replyDataList == null || replyDataList.isEmpty()) {
      meMoValidatorErrors.add(
          new MeMoValidatorError(
              ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND.errorCode()));
      return;
    }

    for (ReplyData replyData : replyDataList) {
      if (replyData.getMessageUUID() == null) {
        meMoValidatorErrors.add(
            new MeMoValidatorError(
                ValidationErrorCode.REPLY_DATA_MESSAGE_UUID_NOT_FOUND.errorCode()));
      }
    }
  }
}
