package dk.digst.digital.post.memolib.container;

import dk.digst.digital.post.memolib.container.Constants.LzmaDictionarySize;
import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.model.Message;
import java.io.IOException;
import java.io.OutputStream;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.compress.compressors.CompressorException;

@UtilityClass
public class MeMoContainerWriterFactory {

  public MeMoContainerWriter newMeMoContainerWriter(OutputStream outputStream)
      throws IOException, CompressorException {

    return new MeMoContainerWriter(new TarLzmaContainerOutputStream(outputStream));
  }

  public MeMoContainerWriter newMeMoContainerWriter(
      OutputStream outputStream, LzmaDictionarySize lzmaDictionarySize)
      throws IOException, CompressorException {

    return new MeMoContainerWriter(
        new TarLzmaContainerOutputStream(outputStream, lzmaDictionarySize));
  }

  public MeMoNewContainerWriter newVersionMeMoContainerWriter(
      OutputStream outputStream, LzmaDictionarySize lzmaDictionarySize)
      throws IOException, CompressorException {

    return new MeMoNewContainerWriter(
        new TarLzmaContainerOutputStream(outputStream, lzmaDictionarySize));
  }

  public MeMoNewContainerWriter newVersionMeMoContainerWriter(OutputStream outputStream)
      throws IOException, CompressorException {

    return new MeMoNewContainerWriter(new TarLzmaContainerOutputStream(outputStream));
  }

  public AbstractContainerWriter<Message> createMeMoContainerWriter(
      @NonNull OutputStream outputStream,
      LzmaDictionarySize lzmaDictionarySize,
      @NonNull MeMoVersion version)
      throws CompressorException, IOException {

    if (version == MeMoVersion.MEMO_VERSION_NEW) {
      if (lzmaDictionarySize == null) {
        return newVersionMeMoContainerWriter(outputStream);
      }
      return newVersionMeMoContainerWriter(outputStream, lzmaDictionarySize);
    } else {
      if (lzmaDictionarySize == null) {
        return newMeMoContainerWriter(outputStream);
      }
      return newMeMoContainerWriter(outputStream, lzmaDictionarySize);
    }
  }
}
