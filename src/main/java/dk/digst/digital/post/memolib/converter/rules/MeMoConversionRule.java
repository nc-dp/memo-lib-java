package dk.digst.digital.post.memolib.converter.rules;

import dk.digst.digital.post.memolib.model.Message;

public interface MeMoConversionRule {
  void convert(Message source);
}
