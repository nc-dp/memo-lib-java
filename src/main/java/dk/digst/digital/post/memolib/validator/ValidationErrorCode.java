package dk.digst.digital.post.memolib.validator;

public enum ValidationErrorCode {
  MEMO_ROOT_INVALID("memo.root.invalid"),
  MEMO_NAMESPACE_NOT_FOUND("memo.namespace.not.found"),
  EMPTY_NOTIFICATION_NOT_ALLOWED("empty.notification.not.allowed"),
  MESSAGE_BODY_NOT_FOUND("message.body.not.found"),
  ID_TYPE_INVALID("id.type.invalid"),
  REPLY_DATA_MESSAGE_UUID_NOT_FOUND("reply.data.message.uuid.not.found"),
  POST_TYPE_INVALID("post.type.invalid"),
  SENDER_CVR_INVALID("sender.cvr.invalid"),
  RECIPIENT_CVR_INVALID("recipient.cvr.invalid"),
  REPRESENTATIVE_CVR_INVALID("representative.cvr.invalid"),
  SENDER_CPR_INVALID("sender.cpr.invalid"),
  RECIPIENT_CPR_INVALID("recipient.cpr.invalid"),
  REPRESENTATIVE_CPR_INVALID("representative.cpr.invalid");

  private final String errorCode;

  ValidationErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String errorCode() {
    return errorCode;
  }
}
