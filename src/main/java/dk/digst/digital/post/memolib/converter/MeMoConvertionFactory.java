package dk.digst.digital.post.memolib.converter;

import dk.digst.digital.post.memolib.converter.rules.MeMoConversionRuleFactory;
import dk.digst.digital.post.memolib.model.MeMoVersion;
import dk.digst.digital.post.memolib.parser.MeMoParserFactory;
import java.io.InputStream;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MeMoConvertionFactory {

  /**
   * Create a up converter that will take a old MeMo and convert it to a new
   *
   * @param inputStream MeMo origin stream
   * @return Up converter
   */
  public static MeMoConverter createUpConverter(InputStream inputStream) {
    return createUpConverter(inputStream, false);
  }

  /**
   * Create a up converter that will take a old MeMo and convert it to a new
   *
   * @param inputStream MeMo origin stream
   * @param enableValidation validate MeMo when parsing
   * @return Up converter
   */
  public static MeMoConverter createUpConverter(InputStream inputStream, boolean enableValidation) {
    MeMoVersion sourceVersion = MeMoVersion.MEMO_VERSION_OLD;
    return new MeMoConverter(
        MeMoConversionRuleFactory.createRules(sourceVersion),
        MeMoParserFactory.createParser(inputStream, enableValidation, sourceVersion));
  }

  /**
   * Create a down converter that will take a new MeMo and convert it to an old
   *
   * @param inputStream MeMo origin stream
   * @return Down converter
   */
  public static MeMoConverter createDownConverter(InputStream inputStream) {
    return createDownConverter(inputStream, false);
  }

  /**
   * Create a down converter that will take a new MeMo and convert it to an old
   *
   * @param inputStream MeMo origin stream
   * @param enableValidation validate MeMo when parsing
   * @return Down converter
   */
  public static MeMoConverter createDownConverter(
      InputStream inputStream, boolean enableValidation) {
    return new MeMoConverter(
        MeMoConversionRuleFactory.createRules(MeMoVersion.MEMO_VERSION_NEW),
        MeMoParserFactory.createNewParser(inputStream, enableValidation));
  }
}
