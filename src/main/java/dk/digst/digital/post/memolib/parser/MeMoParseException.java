package dk.digst.digital.post.memolib.parser;

public class MeMoParseException extends RuntimeException {
  
  public MeMoParseException(String message) {
    super(message);
  }

  public MeMoParseException(Exception e) {
    super(e.getMessage(), e);
  }
}
