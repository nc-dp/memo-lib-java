package dk.digst.digital.post.memolib.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonPropertyOrder({"representativeID"})
public class Representative implements MeMoClass {

  @JacksonXmlProperty(localName = "representativeID")
  private String representativeId;

  private String idType;

  private String idTypeLabel;

  private String label;
}
