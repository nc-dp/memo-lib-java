package dk.digst.digital.post.memolib.validator.rules;

import dk.digst.digital.post.memolib.model.Message;
import dk.digst.digital.post.memolib.model.Recipient;
import dk.digst.digital.post.memolib.model.Sender;
import dk.digst.digital.post.memolib.validator.MeMoValidatorError;
import dk.digst.digital.post.memolib.validator.ValidationErrorCode;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class MeMoIdTypeValidation implements MeMoValidationRule {

  private static final List<String> validIdTypes = Arrays.asList("CPR", "CVR");

  @Override
  public void validate(Message message, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (message.getMessageHeader().getSender() != null) {
      validateSenderIdType(message.getMessageHeader().getSender(), meMoValidatorErrors);
    }
    if (message.getMessageHeader().getRecipient() != null) {
      validateRecipientIdType(message.getMessageHeader().getRecipient(), meMoValidatorErrors);
    }
  }

  private static void validateIdType(
      String idType, String meMoClass, Set<MeMoValidatorError> meMoValidatorErrors) {
    if (!validIdTypes.contains(idType.toUpperCase())) {
      meMoValidatorErrors.add(
          new MeMoValidatorError(
              ValidationErrorCode.ID_TYPE_INVALID.errorCode(), new String[] {meMoClass, idType}));
    }
  }

  private void validateSenderIdType(Sender sender, Set<MeMoValidatorError> meMoValidatorErrors) {
    validateIdType(sender.getIdType(), "sender", meMoValidatorErrors);

    if (sender.getRepresentative() != null) {
      validateIdType(
          sender.getRepresentative().getIdType(), "sender.representative", meMoValidatorErrors);
    }
  }

  private void validateRecipientIdType(
      Recipient recipient, Set<MeMoValidatorError> meMoValidatorErrors) {
    validateIdType(recipient.getIdType(), "recipient", meMoValidatorErrors);
  }
}
