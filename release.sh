set -eu
increment=temp
while getopts ":mpM" opt; do
 case $opt in
  m)
    increment=incrementMinor;;
  p)
    increment=incrementPatch;;
  M)
    increment=incrementMajor;;
  \?)
    echo "Usage: cmd [-M] [-m] [-p]"
  esac
done
./gradlew release -Prelease.versionIncrementer=$increment -Dhttp.proxyHost=localhost -Dhttp.proxyPort=29418
./gradlew build
export VERSION=$(./gradlew -q printVersion)
cd build/libs
BITBUCKET_REPO_OWNER=nc-dp
BITBUCKET_REPO_NAME=memo-lib-java
curl -X POST "https://$BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD@api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_NAME/downloads" --form files=@"memo-lib-java-$VERSION.jar" -v
cd ../..