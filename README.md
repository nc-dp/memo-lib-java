## MeMo-lib
The main purpose of this library is to handle the XML serialization and deserialization of MeMo messages.
It also provides support for handling MeMo messages contained in a zip archive.
___
## Message serialization/deserialization

#### Serialization
##### Building the Message object
To construct the Message object, you can use the provided builder classes which are placed in the 
dk.digst.digital.post.memolib.v1.builder package.

Here is an example of using a builder:

```Java 
MessageBuilder.newBuilder()
  .messageHeader(messageHeader)
  .messageBody(messageBody);
```

##### Handling File content
The MeMo message can contain attached files (pdf, text, html, etc.) and they are contained in the
content field of the File class.
It is possible to include a reference to a file in the file system which will be included in 
the message when the message is serialized.

Example of providing a reference to a file, which will be Base64 encoded by the MeMo lib during 
serialization of the message:

```Java
FileContentBuilder.newBuilder()
  .location("/filesystem/document.pdf");
```

Example of providing the Base64 encoded content:

```Java
FileContentBuilder.newBuilder()
  .base64Content("JVBE");
```

##### Writing the MeMo XML message to an OutputStream
The message can be written to an OutputStream by using the MeMoStreamWriter. Right now only XML is
supported.

```Java
OutputStream output = new FileOutputStream("memo.xml");
MeMoStreamWriter writer = MeMoWriterFactory.createWriter(output);
writer.closeStream();
```

#### Deserialization
A MeMoParser can be used to deserialize a MeMo message. XML Schema validation can be enabled when using
the MeMoParserFactory to instantiate a parser. 

```Java
java.io.File memo = new java.io.File("memo.xml");
FileInputStream fileInputStream = new FileInputStream(memo);                                                            
MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, true);
Message message = parser.parse();
```

#### Validation using MeMo-Lib
A MeMo can be validated according to the now deprecated Schematron. This can be achieved by creating a MeMoValidator. 
There are 2 options, either you create a Validator and just specify the MeMo version, or you create the validator with
a Message. If the validator is created with the Message, validate() can be used without argument. Otherwise the Message
has to be specified.

```Java
/* Read the message */
java.io.File memo = new java.io.File("memo.xml");
FileInputStream fileInputStream = new FileInputStream(memo);                                                            
MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, true);
Message message = parser.parse();

/* Use case 1 - The MeMo version is deduced from the Message object */
MeMoValidator validator = MeMoValidatorFactory(message);
Set<MeMoValidatorError> errors = validator.validate();

/* Use case 2 - Using new MeMo version */
MeMoValidator validator = MeMoValidatorFactory(MeMoVersion.MEMO_VERSION_NEW);
Set<MeMoValidatorError> errors = validator.validate(message);

/* Use case 3 - Using old MeMo version */
MeMoValidator validator = MeMoValidatorFactory(MeMoVersion.MEMO_VERSION_OLD);
Set<MeMoValidatorError> errors = validator.validate(message);
```

##### Streaming using Visitors 
A MeMoParser can also be used to deserialize specific elements of a MeMo message without reading the entire message.
XML Schema validation can be enabled if needed when using the MeMoParserFactory to instantiate the parser. 

The implementation is based on the Visitor pattern, allowing one or more custom visitor(s) to process only those elements that
is needed. The following example shows how to instantiate the parser and read the xml stream:
 
```Java
/* instantiate parser with schema validation enabled */
java.io.File memo = new java.io.File("memo.xml");
FileInputStream fileInputStream = new FileInputStream(memo);
MeMoParser parser = MeMoParserFactory.createParser(fileInputStream, true);

/* traverse the xml stream */
parser.traverse(visitor1, visitor2, visitor3);
```

### Visitors
A visitor must implement the interface MeMoStreamVisitor. The simplest way to create a visitor is to use the MeMoStreamVisitorFactory. 
At the moment, two types of visitors can be created by the factory: Processor and Consumer.

##### Processor
The most simple to use is a processor which implements the interface MeMoStreamProcessorVisitor and can be used to parse certain MeMo classes
within the Message. The factory also provides a method so that the parsed MeMo class can be filtered by the parent element. This could be relevant
if only Files related to a TechnicalDocument should be returned.

Example of creating and using a processor: 
```Java
MeMoStreamProcessorVisitor<AdditionalDocument> visitor1 = MeMoStreamVisitorFactory.createProcessor(AdditionalDocument.class);
parser.traverse(visitor1);
List<AdditionalDocument> additionalDocuments = visitor1.getResult();
```
If we know beforehand, that the processor will only return one element, another option is to call getSingleResult() instead:
```Java
MeMoStreamProcessorVisitor<MessageHeader> visitor2 = MeMoStreamVisitorFactory.createProcessor(MessageHeader.class);
parser.traverse(visitor2);
Optional<MessageHeader> header = visitor2.getSingleResult();
```

##### Consumer
The MeMoStreamVisitorFactory can also create a consumer, which can be used if more control is needed. The visitor must be defined
by a Consumer functional interface/lambda expression which takes a MeMoStreamCursor as parameter. The MeMoStreamCursor provides 
methods to either:
- parse the whole MeMo class including child MeMo classes.
- only parse the elements (simple fields) of the MeMo class.
- traverse the child MeMo classes.

It should be noted, that the state of the underlying xml stream will be affected by these methods since the StAX cursor will move
forward. This means that parsing the whole MeMo class would prevent subsequent traversal of the child MeMo classes.

An example of a visitor, which is only applied to Actions. It parse the elements and collects the startDateTime element.
```Java
List<LocalDateTime> actionStartDateTimes = new ArrayList<>();

MeMoStreamVisitor visitor3 = MeMoStreamVisitorFactory.createConsumer(Action.class,
  cursor -> {
    MeMoClassElements elements = cursor.parseMeMoClassElements();
    elements.get("startDateTime", LocalDateTime.class).ifPresent(startDateTime -> actionStartDateTimes.add(startDateTime));
  });
```
An example of a visitor, which parses the elements an subsequently traverses the child MeMo classes applying other visitors:
```Java
MeMoStreamProcessorVisitor<Action> visitor5 = MeMoStreamVisitorFactory.createProcessor(Action.class);

MeMoStreamVisitor visitor4 = MeMoStreamVisitorFactory.createConsumer( AdditionalDocument.class,
  cursor -> {
    MeMoClassElements elements = cursor.parseMeMoClassElements();
    cursor.traverseChildren(visitor5);
  });    
```

As the consumer does not return a result, it is expected to operate via side-effects (As per the Javadoc for java.util.function.Consumer).
The side effect could be updating the state of an object or adding the result to a collection.

___
### MeMo Container serialization/deserialization

A MeMo container is an archive containing a list of MeMo messages describing the 
contents of the container. As of now, the supported archive format is TAR+LZMA, using compression level 3 and a maximum dictionary size of 128 MB. 

#### Serialization
Example of writing two MeMo messages as entries to the MeMo container:
```Java
OutputStream outputStream = new FileOutputStream("/out/memo.tar.lzma");

/* create the MeMoContainerWriter */
MeMoContainerWriter containerWriter = new MeMoContainerWriterFactory().newMeMoContainerWriter(outputStream);

/* write first entry */
MeMoStreamWriter firstWriter = containerWriter.getWriterForNextEntry("memofile.xml");
firstWriter.write(message1);

/* write second entry */
MeMoStreamWriter secondWriter = containerWriter.getWriterForNextEntry("memofile2.xml");
secondWriter.write(message2);

containerWriter.close();
```

#### Deserialization
Example of reading a MeMo container:
```Java
/* Initialize a container which can be iterated */
FileInputStream fileInputStream = new FileInputStream("/memo.tar.lzma");

/* create reader and */
MeMoContainerReader meMoContainerReader = new MeMoContainerReaderFactory().newMeMoContainerReader(fileInputStream);

/* read each entry */
while(meMoContainerReader.hasEntry()) {
  Optional<Message> message = meMoContainerReader.readEntry();
}
meMoContainerReader.close();
```

#### Creating the MeMo Container from the command line

##### UNIX

The MeMo Container can also be created from the command line, using a command like the following:

```
tar -c MeMo_1.xml MeMo_2.xml | lzma -3 --lzma1=dict=128MB > memo_container.tar.lzma
```

##### Windows

TAR+LZMA files can be produced with various different tools on Windows.
 
* Windows Subsystem for Linux (https://docs.microsoft.com/en-us/windows/wsl/install-win10)
* Cygwin (https://www.cygwin.com)
* MinGW (http://mingw-w64.org/doku.php)
* 7zip (https://www.7-zip.org) + LZMA SDK (https://www.7-zip.org/sdk.html)

Windows Subsystem for Linux, Cygwin and MinGW are all Linux/UNIX environments on Windows, see the UNIX example for information.

7zip and LZMA SDK (also from 7zip) can be used like the following under cmd.exe in Windows (be aware that this is using the Standalone version of 7zip):

```
7za.exe a -ttar -so -an MeMo_1.xml MeMo_2.xml | lzma.exe e -si -so -a0 -lc3 -lp0 -pb2 -fb273 -mc48 -mfhc4 -d27 > memo_container.tar.lzma
```

The above command uses piping between the tar command and the lzma command, using stdin/stdout. 
There are certain limitations in Windows (especially in PowerShell), when using pipes. 
For very large MeMo Containers it might be necessary to split the above example into two commands, that uses the file system as buffer.
Please see 7za.exe and lzma.exe documentation for further information.

Any container with a dictionary size larger than 128 MB will be rejected by NgDP.
